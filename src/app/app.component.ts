import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loggedIn = false;
  token: string = localStorage.getItem('token');
  constructor() {

  }
  ngOnInit() {
    if (window.location.href === localStorage.getItem('path')) {
      localStorage.setItem('entered', 'false');
      localStorage.removeItem('token');
      localStorage.removeItem('path');
    }
    if (this.token != null && localStorage.getItem('entered') === 'true') {
      if (localStorage.getItem('redirectpage') == "redirect to change password page") {
        this.loggedIn = false;
      }
      else if(localStorage.getItem('update') == 'updatePage'){
        this.loggedIn = false
      }
      else if(localStorage.getItem('updated') == 'updated'){
        this.loggedIn = true;
      }
      else {
        this.loggedIn = true;
      }
    } else {
      this.loggedIn = false;
    }

    

  }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServiceService } from '../../../Services/auth_service/auth-service.service';

@Component({
  selector: 'app-master-admin-login',
  templateUrl: './master-admin-login.component.html',
  styleUrls: ['./master-admin-login.component.css']
})
export class MasterAdminLoginComponent implements OnInit {
  MasterAdminLoginForm : FormGroup;
  errorMessage : string;

  constructor(private formBuilder: FormBuilder, private service: AuthServiceService, private routes: Router) { 
    this.MasterAdminLoginForm = this.formBuilder.group ({
      password1 : [''],
      password2 : ['']
    })
  }

  ngOnInit() {
  }

  doLogin(){
    console.log(this.MasterAdminLoginForm.value);
    this.service.doMasterLogin(this.MasterAdminLoginForm.value).subscribe((response) => {
      console.log(response);
      if(response.statusCode == 1){
        localStorage.setItem('token', "jwt")
        window.location.href = "/index";
      }
      else {
        this.errorMessage = "Invalid Credentials supplied or account is locked"
      }
    })
  }
}
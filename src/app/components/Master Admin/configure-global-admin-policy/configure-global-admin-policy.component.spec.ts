import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureGlobalAdminPolicyComponent } from './configure-global-admin-policy.component';

describe('ConfigureGlobalAdminPolicyComponent', () => {
  let component: ConfigureGlobalAdminPolicyComponent;
  let fixture: ComponentFixture<ConfigureGlobalAdminPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureGlobalAdminPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureGlobalAdminPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { FormGroup, FormBuilder } from '@angular/forms';
import { GlobalAdminService } from '../../../Services/global_admin_service/global-admin.service';

@Component({
  selector: 'app-create-global-admin1',
  templateUrl: './create-global-admin1.component.html',
  styleUrls: ['../../../../assets/css/forms.css']
})
export class CreateGlobalAdmin1Component implements OnInit {

  createGlobalAdminForm: FormGroup;
  $: any;
  countries = [];
  issuers: any;
  constructor(private formBuilder: FormBuilder, private service:GlobalAdminService ) {
    this.createGlobalAdminForm = this.formBuilder.group({
      userid: [''],
      firstName: [''],
      middleName: [''],
      lastName: [''],
      description: [''],
      Admins: [''],
      password: [''],
      rePassword: [''],
      changePasswordAtFirstLogin: [''],
      pwdNeverExpires: [''],
      blksimultlogin: [''],
      updateProfileAtLogin: [''],
      maxValForLastPwdList: [''],
      collectAdminContactDetails: [''],
      processorName: [''],
      subProcessorName: [''],
      slt_Countries: [''],
      slt_selCountries: [''],
      slt_Issuers: [''],
      slt_selIssuers: ['']
    })
  }

  ngOnInit() {
    this.getAllCountries();
    this.getAllIssuers();
  }


  getAllCountries() {
    this.service.getAllCountries().subscribe((data: any) => {
      this.countries = data.response;
    })
  }

  getAllIssuers() {
    this.service.getAllIssuers().subscribe((data: any) => {
      this.issuers = data.response;
    })
  }

  ngAfterViewInit() {
    $('.addCountries').on('click', function () {
      var options = $('select.slt_Countries option:selected').sort().clone();
      $('select.slt_selCountries').append(options);
    });

    $('.removeCountries').on('click', function () {
      $('select.slt_selCountries option:selected').remove();
    });


    $('.addIssuers').on('click', function () {
      var options = $('select.slt_Issuers option:selected').sort().clone();
      $('select.slt_selIssuers').append(options);
    });

    $('.removeIssuers').on('click', function () {
      $('select.slt_selIssuers option:selected').remove();
    });
  }

  createGlobalAdmin() {
    console.log(this.createGlobalAdminForm.value);
  }



}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAndUpdateCaseComponent } from './view-and-update-case.component';

describe('ViewAndUpdateCaseComponent', () => {
  let component: ViewAndUpdateCaseComponent;
  let fixture: ComponentFixture<ViewAndUpdateCaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAndUpdateCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAndUpdateCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureAdministratorPrivilegesComponent } from './configure-administrator-privileges.component';

describe('ConfigureAdministratorPrivilegesComponent', () => {
  let component: ConfigureAdministratorPrivilegesComponent;
  let fixture: ComponentFixture<ConfigureAdministratorPrivilegesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureAdministratorPrivilegesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureAdministratorPrivilegesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

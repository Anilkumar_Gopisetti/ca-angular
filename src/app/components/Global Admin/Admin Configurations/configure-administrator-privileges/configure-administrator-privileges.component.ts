import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ConfigureIssuerAdminPrivilegesService } from '../../../../Services/issuer_admin_configuration_service/configure_issuer_admin_privileges/configure-issuer-admin-privileges.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-configure-administrator-privileges',
  templateUrl: './configure-administrator-privileges.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class ConfigureAdministratorPrivilegesComponent implements OnInit {
  configureAdministratorPrivilegesForm: FormGroup;
  privileges: any = [];
  privIds: any = [];
  privType: any = [];
  menuLocation: string;
  privIdsNegative: any = [];
  privTypeNegative: any = [];
  privEnable: any = [];
  privNumberOfAdmins: any = [];
  errorMessage: string;
  privsEnableType: any = [];
  constructor(private service: ConfigureIssuerAdminPrivilegesService, private router : Router ,private formbuilder: FormBuilder) {
    this.configureAdministratorPrivilegesForm = this.formbuilder.group({
      privEnable: [''],
      privNumberOfAdmins: ['']
    })
  }

  ngOnInit() {
    this.fetchAllIssuerAdminData();
  }

  doCancel(){
    this.router.navigate(['/index']);
  }

  fetchAllIssuerAdminData() {
    let array = ["System Configurations", "Issuer Reports", "Issuer Configurations", "Global Admin Configurations", "Issuer Admin Configurations", "Admin Configurations", "Cardholder Configurations", "Callout Configurations", "Enrollment Process Configurations", "Registration Reports", "Transaction Reports", "Admin Reports", "Cardholder Reports", "Upload Configurations", "Template Configurations", "Issuer Reports", "Case Management", "", "", "", "", "External Requests", "Tools"];
    this.service.getAllIssuerPrivilegeData().subscribe((response) => {
      this.privileges = response.response;
      for (let index = 0; index < this.privileges.length; index++) {
        let indexvalue = this.privileges[index].type;
        this.privileges[index].menuid = array[indexvalue - 1];
      }
      this.privilegeCheck();
      this.DualAdminCheck();
      for (let y = 0; y < this.privileges.length; y++) {
        if (this.privileges[y].type < 1) {
          this.configureAdministratorPrivilegesForm.addControl(y.toString(), new FormControl(false));
        }
        else {
          this.configureAdministratorPrivilegesForm.addControl(y.toString(), new FormControl(true));
        }
      }

      for (let y = 0; y < this.privileges.length; y++) {

        if (this.privileges[y].numberOfAdmins >= 2) {
          this.configureAdministratorPrivilegesForm.addControl(this.privileges[y].privilegeId, new FormControl(true));
        }
        else {
          this.configureAdministratorPrivilegesForm.addControl(this.privileges[y].privilegeId, new FormControl(false));
        }
      }
    })


  }

  privilegeCheck() {
    for (let index = 0; index < this.privileges.length; index++) {
      if (this.privileges[index].type < 1) {
        this.privileges[index].checked = false;
      }
      else
        this.privileges[index].checked = true;
    }
  }

  DualAdminCheck() {
    for (let index = 0; index < this.privileges.length; index++) {
      if (this.privileges[index].numberOfAdmins == 2)
        this.privileges[index].dualChecked = true;
      else
        this.privileges[index].dualChecked = false;
    }
  }


  configureAdministratorPrivileges() {
    this.privIds.length = 0;
    this.privType.length = 0;
    this.privEnable.length = 0;
    this.privNumberOfAdmins.length = 0;
    /////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * all privids
     */
    for (let index = 0; index < this.privileges.length; index++) {
      this.privIds.push(this.privileges[index].privilegeId);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////
    for (let i = 0; i < this.privileges.length; i++) {
      if (this.configureAdministratorPrivilegesForm.get(i.toString()).value == true) {
        if (this.privileges[i].type > 0) {
          this.privType.push(this.privileges[i].type);
        }
        else {
          let privtype = (-1 * this.privileges[i].type);
          this.privType.push(privtype);
        }
      }
      else {
        if (this.privileges[i].type < 0) {
          this.privType.push(this.privileges[i].type);
        }
        else {
          let privtype = (-1 * this.privileges[i].type);
          this.privType.push(privtype);
        }
      }
    }

  /////////////////////////////////////////////////////////////////////////////////////////////////////  
    /**
     *  dual controller 
     */
    for (let i = 0; i < this.privileges.length; i++) {
      if (this.configureAdministratorPrivilegesForm.get(this.privileges[i].privilegeId).value == true) {
        let flag = 0;
        if (this.configureAdministratorPrivilegesForm.get(i.toString()).value == true) {
          for (let y = 0; y < this.privEnable.length; y++) {

            if (this.privileges[i].privilegeId == this.privEnable[y]) {
              flag = 1;
            }
          }
          if (flag == 0) {
            this.privNumberOfAdmins.push(this.privileges[i].privilegeId);
          }
        }
      }
    }
    for (let i = 0; i < this.privileges.length; i++) {
      if (this.configureAdministratorPrivilegesForm.get(i.toString()).value == true) {
        this.privEnable.push(this.privileges[i].privilegeId);
      }
    }
//////////////////////////////////////////////////////////////////////////////////////////////////
    this.service.updatePrivileges(this.privIds, this.privType, this.privNumberOfAdmins, this.privEnable).subscribe((data) => {
      if (data.statusCode == 1) {
        this.errorMessage = "Updated Admin Privileges successfully";
      }
      else if (data.statusCode == -1) {
        this.errorMessage = "Error Saving Details";
      }
    });
  }
}

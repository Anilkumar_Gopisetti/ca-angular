import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyAdministratorAccountComponent } from './modify-administrator-account.component';

describe('ModifyAdministratorAccountComponent', () => {
  let component: ModifyAdministratorAccountComponent;
  let fixture: ComponentFixture<ModifyAdministratorAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyAdministratorAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyAdministratorAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ModifyIssuerAdminService } from '../../../../Services/issuer_admin_configuration_service/modify_issuer_admin/modify-issuer-admin.service';

@Component({
  selector: 'app-modify-administrator-account',
  templateUrl: './modify-administrator-account.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class ModifyAdministratorAccountComponent implements OnInit {
  modifyAdministratorForm: FormGroup;
  adminId: boolean = false;
  issuerId: boolean = false;
  admins: any;
  users: any;
  bankcode: any;
  errorMessage: any;
  adminData: any;
  constructor(private formbuilder: FormBuilder, private router : Router ,private service: ModifyIssuerAdminService) {
    
    this.modifyAdministratorForm = this.formbuilder.group({
      issuer: [''],
      Admins: [''],
      remark: [''],
      status: ['']
    })
    this.modifyAdministratorForm.controls['issuer'].setValue("0", { onlySelf: true });
  }

  ngOnInit() {
    this.fetchAllAdmins();
  }

  changeIssuer(value: any) {
    if (value == "0") {
      this.issuerId = false;
      this.errorMessage = "Invalid admin selected."
      this.modifyAdministratorForm.controls['Admins'].setValue("0", { onlySelf: true });
      this.modifyAdministratorForm.controls['status'].setValue("", { onlySelf: true });
      this.modifyAdministratorForm.get('remark').patchValue('');
    }
    else {
      this.issuerId = true;
      this.modifyAdministratorForm.controls['Admins'].setValue("0", { onlySelf: true });
      this.errorMessage = "";
    }
    this.bankcode = this.modifyAdministratorForm.get('issuer').value
    this.fetchAllUsers(this.bankcode);
  }
  changeAdmin(value: any) {
    if (value == 0) {
      this.adminId = false;
    }
    else {
      this.adminId = true;
      this.fetchAdminDetails();
    }
  }

  doCancel(){
    this.router.navigate(['/index']);
  }

  fetchAllAdmins() {
    this.service.getAllAdmins().subscribe((data) => {
      this.admins = data.response
    })
  }

  fetchAllUsers(bankid) {
    this.service.getAllUsers(bankid).subscribe((data) => {
      this.users = data.response;
      if (this.users.length == 0) {
        this.errorMessage = "No Admin(s) found for this admin";
        this.issuerId = false;
      }
      else {
        this.issuerId = true;
        this.errorMessage = " ";
      }
    })
  }

  fetchAdminDetails(){
    let body = {
      adminName : this.modifyAdministratorForm.get('Admins').value,
      bankId : this.modifyAdministratorForm.get('issuer').value
    }
    this.service.getAdminDetails(body).subscribe((data)=>{
      console.log(data)
      if(data.statusCode == 1){
        this.adminData = data.response;
        this.modifyAdministratorForm.controls['status'].setValue(this.adminData.status.toString(), { onlySelf: true })
      this.modifyAdministratorForm.patchValue({
        remark : this.adminData.remarks
      })
      }  
    })
  }

  deleteConfirmation() {
    if (this.modifyAdministratorForm.get('status').value == "3") {
      let message = "Are you sure do you want to remove " + this.modifyAdministratorForm.get('Admins').value;
      let confirm = window.confirm(message)
      if (confirm == true) {
        this.modifyAdministrator();
      }
    }
    else {
      this.modifyAdministrator();
    }
  }

  modifyAdministrator() {
    this.service.modifyGlobalAdmin(this.modifyAdministratorForm.value).subscribe((data) => {
      if (data.statusCode == 1) {
        this.errorMessage = data.response.message;
      }
      else{
        this.errorMessage = "Admin Details not saved Succesfully";
      }
    })

  }
}

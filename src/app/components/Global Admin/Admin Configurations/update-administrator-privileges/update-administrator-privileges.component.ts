import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-update-administrator-privileges',
  templateUrl: './update-administrator-privileges.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class UpdateAdministratorPrivilegesComponent implements OnInit {
  
  updateAdministratorPrivilegesForm : FormGroup;
  
  constructor(private router: Router, private formBuilder : FormBuilder) {
    this.updateAdministratorPrivilegesForm = this.formBuilder.group({
      adminBankId : [''],
      adminIdClear : [''],
      firstNameClear : [''],
      middleNameClear : [''],
      lastNameClear : [''],
      description : [''],
      pwdNeverExpires : [''],
      templateIdClear : [''],
      blockkSimultaneousLogin : [''],
      updateProfileAtLogin : [''],
      maxValForLastPwdList : [''],
      collectAdminContactDetails : [''],
      twoFactorDeliveryChannelType : [''],
      privs : ['']
    })
   }

  ngOnInit() {

  }
  doCanecl() {
    this.router.navigate(['/index']);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAdministratorPrivilegesComponent } from './update-administrator-privileges.component';

describe('UpdateAdministratorPrivilegesComponent', () => {
  let component: UpdateAdministratorPrivilegesComponent;
  let fixture: ComponentFixture<UpdateAdministratorPrivilegesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAdministratorPrivilegesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAdministratorPrivilegesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetAdministratorPasswordComponent } from './reset-administrator-password.component';

describe('ResetAdministratorPasswordComponent', () => {
  let component: ResetAdministratorPasswordComponent;
  let fixture: ComponentFixture<ResetAdministratorPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetAdministratorPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetAdministratorPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

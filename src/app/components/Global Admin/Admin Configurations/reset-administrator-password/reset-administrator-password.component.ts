import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ResetIssuerAdminPasswordService } from '../../../../Services/issuer_admin_configuration_service/reset_issuer_admin_password/reset-issuer-admin-password.service';
import { ErrorMessagesService } from '../../../../Services/service_for_errorMessages/error-messages.service';

@Component({
  selector: 'app-reset-administrator-password',
  templateUrl: './reset-administrator-password.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class ResetAdministratorPasswordComponent implements OnInit {
  resetAdministratorPasswordForm: FormGroup;
  adminSelection: boolean = false;
  admindetails: boolean = false;
  admins: any;
  users: any;
  bankcode: any;
  errorMessage: string;
  errorData: any;

  constructor(private formbuilder: FormBuilder, private router : Router , private service: ResetIssuerAdminPasswordService, private errorService: ErrorMessagesService) {

    this.errorService.getJSON().subscribe((data) => {
      this.errorData = data;
    });

    this.resetAdministratorPasswordForm = this.formbuilder.group({
      newPassword: [''],
      retypeNewpassword: [''],
      remark: [''],
      issuer: [''],
      Admins: [''],
      changePwdAtLogin: ['']
    })

    this.resetAdministratorPasswordForm.controls['Admins'].setValue("0",
      { onlySelf: true });
    this.resetAdministratorPasswordForm.controls['issuer'].setValue("0",
      { onlySelf: true });
  }

  ngOnInit() {
    this.fetchAllAdmins();
  }

  onChangeIssuer(val: any) {
    if (val == 0) {
      this.adminSelection = false;
      this.admindetails = false;
      this.errorMessage = "Invalid admin selected."
    }
    else {
      this.adminSelection = true;
    }
    this.bankcode = this.resetAdministratorPasswordForm.get('issuer').value
    this.fetchAllUsers(this.bankcode);
  }

  onChangeAdmins(val: any) {
    if (val == 0) {
      this.admindetails = false;
    }
    else {
      this.admindetails = true;
    }

  }

  doCancel(){
    this.router.navigate(['/index']);
  }

  fetchAllAdmins() {
    this.service.getAllAdmins().subscribe((data) => {
      this.admins = data.response;
    })
  }
  fetchAllUsers(bankid) {

    this.service.getAllUsers(bankid).subscribe((data) => {
      this.users = data.response;
      if (this.users.length == 0) {
        this.errorMessage = "No Admin(s) found for this admin";
        this.adminSelection = false;
      }
      else{
        this.adminSelection = true;
        this.errorMessage = " ";
      }
    })
  }
  ResetAdministratorPassword() {
    this.service.resetIssuerAdmin(this.resetAdministratorPasswordForm.value).subscribe((data) => {
      if (data.statusCode == 1) {
        this.errorMessage = "Reset Admin Password Successful";
      }
      else if (data.statusCode == -10) {
        this.errorMessage = "Something went wrong";
      }
      else {
        // this.errorMessage = "Admin Profile not Updated Successfully";
        for (let i = 0; i < this.errorData.length; i++) {
          if (this.errorData[i].errorCode == data.response) {
            this.errorMessage = this.errorData[i].errorMessage;
            break;
          }
        }
      }
    })
  }
}
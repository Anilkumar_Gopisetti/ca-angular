import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalAdminService } from '../../../../Services/global_admin_service/global-admin.service';
import { CreateIssuerAdminService } from '../../../../Services/issuer_admin_configuration_service/create_issuer_admin/create-issuer-admin.service';

@Component({
  selector: 'app-create-administrator',
  templateUrl: './create-administrator.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class CreateAdministratorComponent implements OnInit {
  issuerId: boolean = false;
  templateId: boolean = false;
  default: any = 0;
  defaultcollectAdminContactDetails: any = 0;
  privileges: any = [];
  isPrivilege: boolean = false;
  checkAllPrivileges: boolean = false;
  checked: boolean = true;
  errorMessage: string;
  issuers: any;
  numbers:any = ["1","2","3","4","5","6","7","8","9","10","11","12","13"];
  createAdministratorForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private router : Router , private service: GlobalAdminService, private createIssuerService: CreateIssuerAdminService) {

    this.createAdministratorForm =
      this.formBuilder.group({
        Admins: [''],
        issuer: [''],
        userid: [''],
        firstName: [''],
        middleName: [''],
        lastName: [''],
        description: [''],
        password: [''],
        rePassword: [''],
        changePasswordAtFirstLogin: [''],
        pwdNeverExpires: [''],
        blksimultlogin: [''],
        updateProfileAtLogin: [''],
        twoFactorAuthenticationType: [''],
        maxValForLastPwdList: [''],
        collectAdminContactDetails: [''],
        privs: ['']
      })
    this.createAdministratorForm.controls['Admins'].setValue("None", { onlySelf: true });
    this.createAdministratorForm.controls['maxValForLastPwdList'].setValue("0", { onlySelf: true });
  }
  ngOnInit() {
    this.defaultcollectAdminContactDetails = 0;
    this.fetchIssuers();
    this.fetchIssuerPrevileges();
    for (let i = 0; i < 143; i++) {
      this.createAdministratorForm.addControl(i.toString(), new FormControl(''));
    }
  }

  

  changeIssuer(value: any) {
    if (value == 0) {
      this.issuerId = false;
      this.errorMessage = "Invalid Issuer selected";
    }
    else {
      this.issuerId = true;
      this.errorMessage = "";
      this.createAdministratorForm.get('collectAdminContactDetails').disable();
    }

  }

  ChangeTemplate(value: any) {
    if (value == 0) {
      this.templateId = false;
    } else {
      this.templateId = true;
    }
  }

  allPrivileges(event) {
    const checked = event.target.checked;
    this.checked = checked
  }

  changePrivilegesByCategory(event) {

    if (event.target.name == 'privileges') {
      this.isPrivilege = true
    }
    if (this.isPrivilege && this.checkAllPrivileges) {
      event.target.checked = true
    }
  }

  fetchIssuers() {
    this.service.getAllIssuers().subscribe((response) => {
      console.log(response);
      this.issuers = response.response;
    })
  }

  doCancel(){
    this.router.navigate(['/index']);
  }

  fetchIssuerPrevileges() {
    this.createIssuerService.getIssuerPrivilegeList().subscribe((data) => {
      this.privileges = data.response;
      console.log(this.privileges);
    })
  }

  createAdministrator() {
  
    let privilegesList = [];
    for (let i = 0; i < this.privileges.length; i++) {
      let y = i.toString();

      if (this.createAdministratorForm.get(y).value == true) {
        privilegesList.push(this.privileges[i].privilegeId);
      }
    }

    if (this.createAdministratorForm.get('privs').value == true) {
      privilegesList.length = 0;
      privilegesList = this.privileges;
    }
    if(this.createAdministratorForm.get('privs').touched == false){
      for (let i = 0; i < this.privileges.length; i++) {
            privilegesList.push(this.privileges[i].privilegeId)
          }
    }  


    this.createAdministratorForm.patchValue({
      privs: privilegesList,
    });
    this.createIssuerService.createIssuer(this.createAdministratorForm.value).subscribe((response) => {
      if(response.statusCode == 1){
        this.errorMessage = "Created Administrator successfully";
      }if(response.statusCode == 0){
        this.errorMessage = response.response;
      }
      console.log(response);
    })
  }
}

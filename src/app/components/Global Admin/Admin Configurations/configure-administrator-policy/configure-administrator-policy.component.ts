import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ConfigureIssuerAdminPolicyService } from '../../../../Services/issuer_admin_configuration_service/configure_issuer_admin_policy/configure-issuer-admin-policy.service';

@Component({
  selector: 'app-configure-administrator-policy',
  templateUrl: './configure-administrator-policy.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class ConfigureAdministratorPolicyComponent implements OnInit {
  
  issuerId: boolean = false;
  configureAdministratorForm: FormGroup;
  errorMessage : string;
  issuersList : any;
  constructor(private formBuilder: FormBuilder,private router : Router ,private service:ConfigureIssuerAdminPolicyService ) {
    this.configureAdministratorForm = this.formBuilder.group({
      issuer: [''],
      MaxTriesPerSession: ['3'],
      MaxTriesAcrossSession: ['6'],
      MinLength: [''],
      MaxLength: [''],
      MinNumeric: [''],
      MinAlphabet: [''],
      MinSpecialCharacters: [''],
      MinUpperCaseCharacters: [''],
      MinLowerCaseCharacters: [''],
      PasswordRenewalFrequency: ['60'],
      MaxInactivityPeriod: ['60'],
      AllowPasswordReset: [''],
      loginSessionTimeout: ['0'],
      passwordStorageAlgorithm: [''],
      passwordChangeVelocity: ['0'],
      collectAdminContactDetails: [''],
      twoFactorDeliveryChannelType: ['']
    })
    this.configureAdministratorForm.controls['passwordStorageAlgorithm'].setValue("0", { onlySelf: true });
    this.configureAdministratorForm.controls['collectAdminContactDetails'].setValue("0", { onlySelf: true });
    this.configureAdministratorForm.controls['twoFactorDeliveryChannelType'].setValue("0", { onlySelf: true });

  }

  ngOnInit() {
    this.fetchAllIssuers();
  }

  changeIssuer(value: any) {
    if (value == 0) {
      this.issuerId = false;
    }
    else {
      this.issuerId = true;
    }
  }

  doCance(){
    this.router.navigate(['/index'])
  }

  fetchAllIssuers(){
    this.service.getAllIssuers().subscribe((issuers) => {
      this.issuersList = issuers.response;
      console.log(this.issuersList);
    })
  }

  configureAdministrator() {
    let confirm = window.confirm('Password Policy is weaker than default password policy. Do you want to continue ?');
    if(confirm == true){
    this.service.configureIssuer(this.configureAdministratorForm.value).subscribe((response)=>{
      if(response.statusCode == 1){
        this.errorMessage = "Configured Admin Parameters successfully";
      }
      else if(response.statusCode == -1){
        this.errorMessage = "Login session timeout should be numeric";
      } 
      else if(response.response == "Invalid User !!!"){
        alert("Session Expired");
        window.location.href = "/";
      }
      else {
        this.errorMessage = "One or more input was an invalid number"
      }
    })
  }
}

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureAdministratorPolicyComponent } from './configure-administrator-policy.component';

describe('ConfigureAdministratorPolicyComponent', () => {
  let component: ConfigureAdministratorPolicyComponent;
  let fixture: ComponentFixture<ConfigureAdministratorPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureAdministratorPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureAdministratorPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

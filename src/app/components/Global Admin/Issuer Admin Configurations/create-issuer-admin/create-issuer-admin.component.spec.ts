import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateIssuerAdminComponent } from './create-issuer-admin.component';

describe('CreateIssuerAdminComponent', () => {
  let component: CreateIssuerAdminComponent;
  let fixture: ComponentFixture<CreateIssuerAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateIssuerAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateIssuerAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

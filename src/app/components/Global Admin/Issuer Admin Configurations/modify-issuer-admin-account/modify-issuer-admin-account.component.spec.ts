import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyIssuerAdminAccountComponent } from './modify-issuer-admin-account.component';

describe('ModifyIssuerAdminAccountComponent', () => {
  let component: ModifyIssuerAdminAccountComponent;
  let fixture: ComponentFixture<ModifyIssuerAdminAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyIssuerAdminAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyIssuerAdminAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

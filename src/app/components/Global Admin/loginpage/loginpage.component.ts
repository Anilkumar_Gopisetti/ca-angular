import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServiceService } from '../../../Services/auth_service/auth-service.service';
@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.css']
})
export class LoginpageComponent implements OnInit {
  loginForm: FormGroup;
  errorMessage: string;
  showPopup: boolean = true;
  sessionData: any;
  constructor(private formBuilder: FormBuilder, private service: AuthServiceService, private routes: Router) {
    this.loginForm = this.formBuilder.group({
      adminName: [''],
      password: ['']
    })
  }
  ngOnInit() {
  }

  doLogin() {
    localStorage.setItem('password',this.loginForm.get('password').value);
    localStorage.setItem('username',this.loginForm.get('adminName').value)
    this.service.doLogin(this.loginForm.value).subscribe((response) => {
      if (response.statusCode == 1) {
        this.sessionData = response;
        localStorage.setItem('token', response.token);
        localStorage.setItem('entered', "true");
        localStorage.setItem('path', window.location.href);
        localStorage.setItem('sessionData',JSON.stringify(this.sessionData) )
        window.location.href = "/index";
      }
      else if (response.statusCode == 2) {
        this.sessionData = response;
        localStorage.setItem('token', response.token);
        localStorage.setItem('entered', "true");
        localStorage.setItem('path', window.location.href);
        localStorage.setItem('sessionData',JSON.stringify(this.sessionData) )
        window.location.href = "/update_profile/first_login";
      }
      else if (response.statusCode == 3) {
        this.sessionData = response;
        localStorage.setItem('token', response.token);
        localStorage.setItem('entered', "true");
        localStorage.setItem('path', window.location.href);
        localStorage.setItem('sessionData',JSON.stringify(this.sessionData) )
        window.location.href = "/index";
      }
      else if (response.statusCode == 4) {
        this.sessionData = response;
        localStorage.setItem('token', response.token);
        localStorage.setItem('entered', "true");
        localStorage.setItem('path', window.location.href);
        localStorage.setItem('redirectpage',response.response);
        localStorage.setItem('sessionData',JSON.stringify(this.sessionData) )
        if("redirect to change password page" == response.response){
          window.location.href = "/change_password";
        }
      }
      else if (response.statusCode == -3) {
        this.errorMessage = "We are sorry, the Database Service is currently not available, please try after some time";
      }
      else {
        this.errorMessage = "Invalid Credentials supplied or account is locked"
      }
    })
  }
  hidePopup() {
    this.showPopup = false;
  }
}

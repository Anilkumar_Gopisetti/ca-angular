import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ModifyGlobalAdminService } from '../../../../Services/global_admin_service/modify_global_admin/modify-global-admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modify-global-admin',
  templateUrl: './modify-global-admin.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class ModifyGlobalAdminComponent implements OnInit {
  changeId: boolean = false
  modifyGlobalForm: FormGroup;
  errorMessage: any;
  admins:any;
  adminData : any;
  constructor(private formBuilder: FormBuilder, private router : Router ,private service:ModifyGlobalAdminService) {
    this.modifyGlobalForm = this.formBuilder.group({
      selectedAdminName: [''],
      remark: [''],
      status: ['']
    })
  }

  ngOnInit() {
    this.fetchAllAdmins();
  }
  changeAdmin(value:any) {
    if(value==0){
      this.changeId = false;
      this.errorMessage = "Invalid admin selected.​";
    }
    else{
      this.changeId = true;
      this.errorMessage = "";
      this.fetchAdminDetails();
    }
  }
  
  fetchAllAdmins(){
    this.service.getAllAdmins().subscribe((data) => {
      console.log(data);
      this.admins = data.response;
    })
  }

  fetchAdminDetails(){
    this.service.getAdminDetails(this.modifyGlobalForm.get('selectedAdminName').value).subscribe((data)=>{
      if(data.statusCode == 1){
        this.adminData = data.response;
        this.modifyGlobalForm.controls['status'].setValue(this.adminData.status.toString(), { onlySelf: true })
      this.modifyGlobalForm.patchValue({
        remark : this.adminData.remarks
      })
      }  
    })
  }

  doCancel(){
    this.router.navigate(['/index']);
  }

  deleteConfirmation() {
    if (this.modifyGlobalForm.get('status').value == "3") {
      let message = "Are you sure do you want to remove " + this.modifyGlobalForm.get('selectedAdminName').value;
      let confirm = window.confirm(message)
      if (confirm == true) {
        this.modifyGlobalAdmin();
      }
    }
    else {
      this.modifyGlobalAdmin();
    }
  }

  modifyGlobalAdmin() {
    this.service.modifyGlobalAdmin(this.modifyGlobalForm.value).subscribe((data)=>{
      if(data.statusCode==1){
        this.errorMessage=data.response.message;
      }
      else{
        this.errorMessage = "Admin Details not saved Succesfully"
      }
    })
  }

}

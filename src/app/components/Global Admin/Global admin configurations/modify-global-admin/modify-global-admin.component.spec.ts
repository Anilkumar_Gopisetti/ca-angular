import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyGlobalAdminComponent } from './modify-global-admin.component';

describe('ModifyGlobalAdminComponent', () => {
  let component: ModifyGlobalAdminComponent;
  let fixture: ComponentFixture<ModifyGlobalAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyGlobalAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyGlobalAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

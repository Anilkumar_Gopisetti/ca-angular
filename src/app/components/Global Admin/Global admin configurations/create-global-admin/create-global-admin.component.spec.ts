import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateGlobalComponent } from './create-global-admin.component';

describe('CreateGlobalComponent', () => {
  let component: CreateGlobalComponent;
  let fixture: ComponentFixture<CreateGlobalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateGlobalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGlobalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

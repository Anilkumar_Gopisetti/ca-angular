import { Component, OnInit } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core'
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as $ from 'jquery';
import { UpdateGlobalAdminPrivilegesService } from '../../../../Services/global_admin_service/update_global_admin_privileges/update-global-admin-privileges.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-global-admin',
  templateUrl: './update-global-admin.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']

})
export class UpdateGlobalAdminComponent implements OnInit {

  globalAdminUpdateForm: FormGroup;
  $: any;
  showDetails: boolean = false;
  templateId: boolean = false;
  users: any;
  userdata: string;
  _values2 = [];
  banklist: any = [];
  adminId: boolean = false;
  loggedInAdminsPrivilegesLength = 0;
  adminDetails: any;
  loggedInPrivileges: any;
  adminPrivileges: any;
  privilegesSize: number;
  isPrivilege: boolean = false;
  checkAllPrivileges: boolean = false;
  selectedIssuers: any = [];
  selectedRemoveIssuers: any = [];
  issuerCode: any = [];
  errorMessage: any;
  constructor(private formBuilder: FormBuilder, private router : Router ,private service: UpdateGlobalAdminPrivilegesService, private ref: ChangeDetectorRef) {
    this.globalAdminUpdateForm = this.formBuilder.group({
      userid: [''],
      firstName: [''],
      middleName: [''],
      lastName: [''],
      description: [''],
      templateIdClear: ['',],
      password: [''],
      rePassword: [''],
      changePasswordAtFirstLogin: [''],
      pwdNeverExpires: [''],
      blksimultlogin: [''],
      updateProfileAtLogin: [''],
      maxValForLastPwdList: [''],
      collectAdminContactDetails: [''],
      twoFactorDeliveryChannelType: [''],
      processorName: [''],
      subProcessorName: [''],
      slt_Issuers: [''],
      slt_selIssuers: [''],
      userEncoding: [''],
      privs: ['']

    })
    this.globalAdminUpdateForm.controls['userEncoding'].setValue("--", { onlySelf: true })
  }

  ngOnInit() {

    this.getAllUserIds();
    this.getallbanks();
  }

  _values1 = [
    { id: 0, name: "Do not collect" },
    { id: 1, name: "Only Email Address" },
    { id: 2, name: "Only Phone Number" },
    { id: 3, name: "Both Email Address and Phone Number" }
  ];

  firstDropDownChanged(val: any) {
    const obj = this._values1[val];
    if (!obj) return;
    if (obj.id == 1) {
      this._values2 = [
        { id: "2", name: "EMAIL" }
      ];
    } else if (obj.id == 2) {
      this._values2 = [
        { id: "1", name: "SMS" },
        { id: "2", name: "VOICE" }
      ];
    } else if (obj.id == 3) {
      this._values2 = [
        { id: "1", name: "EMAIL and SMS" },
        { id: "2", name: "SMS" },
        { id: "3", name: "EMAIL" },
        { id: "4", name: "VOICE" },
        { id: "5", name: "EMAIL AND VOICE" }
      ];
    }
    else {
      this._values2 = [];
    }
  }

  getUnique(array) {
    var uniqueArray = [];
    for (let i = 0; i < array.length; i++) {
      if (uniqueArray.indexOf(array[i]) == -1) {
        uniqueArray.push(array[i]);
      }
    }
    return uniqueArray;
  }

  addIssuers() {
  console.log(this.globalAdminUpdateForm.get('slt_Issuers').value);
    console.log(this.selectedIssuers);
    for (let i = 0; i < this.globalAdminUpdateForm.get('slt_Issuers').value.length; i++) {
      this.selectedIssuers.push(this.globalAdminUpdateForm.get('slt_Issuers').value[i]);
    }
    this.selectedIssuers = this.getUnique(this.selectedIssuers);
  }

  removeIssuers() {
    this.selectedRemoveIssuers.length = 0;
    for (let i = 0; i < this.globalAdminUpdateForm.get('slt_selIssuers').value.length; i++) {
      this.selectedRemoveIssuers.push(this.globalAdminUpdateForm.get('slt_selIssuers').value[i]);
    }
    for (let j = 0; j < this.selectedRemoveIssuers.length; j++) {
      this.selectedIssuers.splice($.inArray(this.selectedRemoveIssuers[j], this.selectedIssuers), 1)
    }
  }

  getAllUserIds() {

    this.service.getallUsers().subscribe((data: any) => {

      this.users = data.response;

    })
  }
  getallbanks() {

    this.service.getallbanks().subscribe((data: any) => {
      if (data.statusCode == 1) {
        this.banklist = data.response;
      }
      console.log(this.banklist);
    })
  }

  doCancel(){
    this.router.navigate(['/index']);
  }

  changeTemplate(value: any) {
    if (value == 0) {
      this.templateId = false;
      this.globalAdminUpdateForm.get('blksimultlogin').enable();
      this.globalAdminUpdateForm.get('updateProfileAtLogin').enable();
    } else {
      this.templateId = true;
      this.globalAdminUpdateForm.get('pwdNeverExpires').patchValue("");
      this.globalAdminUpdateForm.get('blksimultlogin').patchValue("");
      this.globalAdminUpdateForm.get('updateProfileAtLogin').patchValue("");
      this.globalAdminUpdateForm.get('blksimultlogin').disable();
      this.globalAdminUpdateForm.get('updateProfileAtLogin').disable();
    }

  }
  showDeliveryChannel(val: any) {
    const obj = this._values1[val];
    if (obj.id == 0) {
      this.adminId = false;
    }
    else {
      this.adminId = true;
    }
  }

  changeAdmin(userId: any) {
    let issuers = []
    this.service.getAdminDetails(this.globalAdminUpdateForm.get('userEncoding').value).subscribe((adminDetails) => {
      if (adminDetails.statusCode == 2) {

        this.adminDetails = adminDetails;
        issuers = adminDetails.response.authBanksOfSelAdmin;

        for (let i = 0; i < issuers.length; i++) {
          this.selectedIssuers.push(issuers[i].label);
        }
        if (userId == 0) {
          this.showDetails = false;
        } else {
          this.showDetails = true;
        }
        this.globalAdminUpdateForm.patchValue({
          slt_selAdmins: adminDetails.response.adminInput.authBanksOfSelAdmin,
          firstName: adminDetails.response.adminInput.firstNameClear,
          middleName: adminDetails.response.adminInput.middleNameClear,
          lastName: adminDetails.response.adminInput.lastNameClear,
          description: adminDetails.response.adminInput.description,
          pwdNeverExpires: adminDetails.response.adminInput.pwdNeverExpires,
          blksimultlogin: adminDetails.response.adminInput.blockkSimultaneousLogin,
          divTwoFactorAuthTypeLabel: adminDetails.response.adminInput.twoFactorAuthenticationEnabled,
          maxValForLastPwdList: adminDetails.response.adminInput.maxValForLastPwdList,
          templateIdClear: adminDetails.response.adminInput.templateIdClear,
          processorName: adminDetails.response.adminInput.processorName,
          subProcessorName: adminDetails.response.adminInput.subProcessorName,
          updateProfileAtLogin: adminDetails.response.adminInput.updateProfileAtLogin,
          collectAdminContactDetails: adminDetails.response.adminInput.collectAdminContactDetails,
          twoFactorDeliveryChannelType: adminDetails.response.adminInput.twoFactorDeliveryChannelType
        });
        this.settingDefaultFormValues();
        this.loggedInPrivileges = this.adminDetails.response.loggedInAdminsPrivileges;
        this.adminPrivileges = this.adminDetails.response.selectedAdmin.privilegeIDList;

        if (this.privilegesSize > 0) {
          for (let remove = 0; remove < this.privilegesSize; remove++) {
            this.globalAdminUpdateForm.removeControl(remove.toString());
          }
        }
        this.privilegesSize = this.adminDetails.response.loggedInAdminsPrivileges.length;

        for (let j = 0; j < this.loggedInPrivileges.length; j++) {
          let flag = 0;
          for (let i = 0; i < this.adminPrivileges.length; i++) {
            if (this.loggedInPrivileges[j].privilegeId == this.adminPrivileges[i]) {
              this.loggedInPrivileges[j].checked = true;
              flag = 1;
            }
          }
          if (flag == 0) {
            this.loggedInPrivileges[j].checked = false;
          }
        }
        for (let i = 0; i < this.privilegesSize; i++) {

          this.globalAdminUpdateForm.addControl(i.toString(), new FormControl(this.loggedInPrivileges[i].checked));
        }
      }
    })
  }
  settingDefaultFormValues() {
    this.globalAdminUpdateForm.controls['templateIdClear'].setValue("-1000", { onlySelf: true });
    if (this.adminDetails.response.adminInput.collectAdminContactDetails != 0) {
      this.globalAdminUpdateForm.controls['collectAdminContactDetails'].setValue(this.adminDetails.response.adminInput.collectAdminContactDetails, { onlySelf: true });
      this.adminId = true;
      this.firstDropDownChanged(this.adminDetails.response.adminInput.collectAdminContactDetails);
      this.globalAdminUpdateForm.controls['twoFactorDeliveryChannelType'].setValue(this.adminDetails.response.adminInput.twoFactorDeliveryChannelType, { onlySelf: true })
    }
    else {
      this.globalAdminUpdateForm.controls['collectAdminContactDetails'].setValue(0, { onlySelf: true });
      this.adminId = false;
      this.templateId = false;
    }
    this.globalAdminUpdateForm.controls['processorName'].setValue(this.adminDetails.response.adminInput.processorName, { onlySelf: true });
    this.globalAdminUpdateForm.controls['subProcessorName'].setValue(this.adminDetails.response.adminInput.subProcessorName, { onlySelf: true });
    this.globalAdminUpdateForm.controls['maxValForLastPwdList'].setValue(this.adminDetails.response.adminInput.maxValForLastPwdList, { onlySelf: true });

  }

  allPrivileges(event) {

    const checked = event.target.checked;
    let privileges = {};
    for (let index = 0; index < this.loggedInPrivileges.length; index++) {
      this.loggedInPrivileges[index].checked = checked;
    }

    for (let i = 0; i < this.privilegesSize; i++) {
      if (this.globalAdminUpdateForm.get('privs').value == true) {
        let property = i.toString();
        privileges[property] = true;
      }
      else {
        let property = i.toString();;
        privileges[property] = false;
      }
    }
    this.globalAdminUpdateForm.patchValue(privileges);

  }

  updateGlobalAdmin() {
    console.log(this.globalAdminUpdateForm.value);
    let updatedPrivileges = []
    if (this.globalAdminUpdateForm.get('privs').value == false) {
      for (let i = 0; i < this.privilegesSize; i++) {
        if (this.globalAdminUpdateForm.get(i.toString()).value == true) {
          updatedPrivileges.push(this.loggedInPrivileges[i].privilegeId)
        }
      }
    }
    else {
      for (let i = 0; i < this.privilegesSize; i++) {
        updatedPrivileges.push(this.loggedInPrivileges[i].privilegeId);
      }
    }
    for (let i = 0; i < this.selectedIssuers.length; i++) {
      for (let j = 0; j < this.banklist.length; j++) {
        if (this.banklist[j].bankName == this.selectedIssuers[i]) {
          this.issuerCode.push(this.banklist[j].bankCode.toString())
        }
      }
    }
    this.issuerCode = this.getUnique(this.issuerCode);

    this.globalAdminUpdateForm.patchValue({
      privs: updatedPrivileges,
      slt_selIssuers: this.issuerCode
    })
    console.log(this.globalAdminUpdateForm.value)
    this.service.updateGLobalAdminPrivileges(this.globalAdminUpdateForm.value).subscribe((data) => {
      if (data.statusCode == 2) {
        this.errorMessage = data.response;
      }
    })
  }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ResetGlobalAdminPasswordService } from '../../../../Services/global_admin_service/reset_global_admin_password/reset-global-admin-password.service';
import { ErrorMessagesService } from '../../../../Services/service_for_errorMessages/error-messages.service';

@Component({
  selector: 'app-reset-global-admin',
  templateUrl: './reset-global-admin.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})

export class ResetGlobalAdminComponent implements OnInit {

  changeId: boolean = false;
  resetPasswordForm: FormGroup;
  admins: any=[];
  errorMessage: string;
  adminName: any;
  errorData : any;

  constructor(private formbuilder: FormBuilder, private router : Router , private service: ResetGlobalAdminPasswordService, private errorService : ErrorMessagesService) {

    this.errorService.getJSON().subscribe((data) => {      
      this.errorData = data;
    });

    this.resetPasswordForm = this.formbuilder.group({
      newPassword: [''],
      retypeNewpassword: [''],
      remark: [''],
      Admins: [''],
      changePwdAtLogin: [''],

    })

    this.resetPasswordForm.controls['Admins'].setValue("0",{ onlySelf: true });
  }

  ngOnInit() {
    this.fetchAllAdmins();
  }

  changeAdmin(value: any) {
    if (value == 0) {
      this.changeId = false;
      this.errorMessage = "Invalid admin selected."
      this.resetPasswordForm.get('newPassword').patchValue('');
      this.resetPasswordForm.get('retypeNewpassword').patchValue('');
      this.resetPasswordForm.get('remark').patchValue('');
    }
    else {
      this.changeId = true
    }
    this.adminName = this.resetPasswordForm.get('Admins').value
  }

  fetchAllAdmins() {
    this.service.getAllAdmins().subscribe((data) => {
      this.admins = data.response;
      console.log(this.admins)
    })
  }

  cancelOperation(){
    this.router.navigate(['/index']);
  }

  resetPassword() {
  let bankName:any;
 for(let i=0;i<this.admins.length;i++){
   if(this.adminName == this.admins[i].adminName){
    bankName=this.admins[i].adminName
   }
 }
    this.resetPasswordForm.patchValue({
      Admins: bankName
    })

    this.service.resetGlobalAdmin(this.resetPasswordForm.value).subscribe((data) => {
      if (data.statusCode == 1) {
        this.errorMessage = "Reset Admin Password successful";
      }  
      else{
        for(let i=0;i<this.errorData.length;i++)
      {
        if(this.errorData[i].errorCode == data.response)
        {
          this.errorMessage = this.errorData[i].errorMessage;
        break;
      }
      else{
        this.errorMessage = "Two Passwords should match and can not be blank"
      }
      }
      }
      if (data.statusCode == -4) {
        this.errorMessage = "Password Policy for this level is not satisfied";
      }
    })
  }




}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UploadSigningCertificateService } from '../../../../Services/issuer_configuration_service/upload_signing_certificate_service/upload-signing-certificate.service';

@Component({
  selector: 'app-upload-signing-certificate',
  templateUrl: './upload-signing-certificate.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class UploadSigningCertificateComponent implements OnInit {
  uploadSigningCertificateForm:FormGroup
  constructor(private formBuilder : FormBuilder, private service : UploadSigningCertificateService) { }

  ngOnInit() {
  }
  uploadSigningCertificate(){
    this.service
  }

}

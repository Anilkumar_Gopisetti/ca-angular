import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UpdateIssuerService } from '../../../../Services/issuer_configuration_service/update_issuer_service/update-issuer.service';

@Component({
  selector: 'app-update-issuer',
  templateUrl: './update-issuer.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class UpdateIssuerComponent implements OnInit {

  updateIssuerForm: FormGroup;
 

  constructor(private service: UpdateIssuerService,private formBuilder : FormBuilder) {
    this.updateIssuerForm = this.formBuilder.group({
      bankName : [''],
      country : [''],
      localeID : [''],
      localTimeZone : [''],
      customerId : [''],
      bankDirName : [''],
      issuerDisplayName : [''],
      encryptionKey : [''],
      uploadKey : [''],
      uploadKeyHidden : [''],
      retypeUploadKey : [''],
      userEncoding : [''],
      cvvKeyA : [''],
      cvvKeyB : [''],
      bankKeyModule : [''],
      arKeyModule : [''],
      processorName : [''],
      subProcessorName : [''],
      processorData : [''],
      processorInfo : [''],
      CVVKEYIND : [''],
      DoNotPromptBehavior : [''],
      enrollmentLocales : [''],
      userIdEnabled : [''],
      twoStepLoginEnabled : [''],
      dualAuthRequiredAtCommit : [''],
      collectAdminContactDetails : [''],
      slt_Admins : [''],
      slt_selAdmins : [''],
    })
   }

  ngOnInit() {
  }
  updateIssuer(){
    this.service.updateIssuer(this.updateIssuerForm.value);
  }
  
}

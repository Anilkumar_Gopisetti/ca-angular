import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AddIssuerCustomizationService } from '../../../../Services/issuer_configuration_service/add_issuer_customization_service/add-issuer-customization.service';

@Component({
  selector: 'app-add-issuer-customization',
  templateUrl: './add-issuer-customization.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class AddIssuerCustomizationComponent implements OnInit {
  addIssuerCustomizationForm:FormGroup
  constructor(private service : AddIssuerCustomizationService ) { }

  ngOnInit() {
  }
  addIssuerCustomization(){
this.service.addIssuerCustomization()
  }
}

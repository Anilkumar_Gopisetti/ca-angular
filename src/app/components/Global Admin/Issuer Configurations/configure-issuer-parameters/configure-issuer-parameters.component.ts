import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-configure-issuer-parameters',
  templateUrl: './configure-issuer-parameters.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class ConfigureIssuerParametersComponent implements OnInit {

  showParametersForm : boolean = false;
  configureissuerParameterForm : FormGroup;
  IsserForm : FormGroup;
  constructor(private formBuilder : FormBuilder) { 
    this.IsserForm = this.formBuilder.group({
      SecondaryBank : ['']
    })
   }
  ngOnInit() {
  }

  onIssuerSelect(){
    alert(this.IsserForm.get('SecondaryBank').value);
    if (this.IsserForm.get('SecondaryBank').value == -1){
      this.showParametersForm = false;
    }
    else {
      this.showParametersForm = true;
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as $ from 'jquery';
import { CreateIssuerService } from '../../../../Services/issuer_configuration_service/create_issuer/create-issuer.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-create-issuer',
  templateUrl: './create-issuer.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class CreateIssuerComponent implements OnInit {
  createIssuerForm:FormGroup;
   $: any;
   countries:any;
   issuers:any;
   locales : any;
   admins :any;
   errorMessage : string;
   selectedAdmins :any = [];
   selectedRemoveAdmins: any = [];
   timeZones : any;
  constructor( private formBuilder : FormBuilder, private service : CreateIssuerService, private http : HttpClient ) { 
    this.createIssuerForm = this.formBuilder.group({
      bankName : [''],
      country : [''],
      localeID : [''],
      localTimeZone : [''],
      customerId : [''],
      bankDirName : [''],
      issuerDisplayName : [''],
      encryptionKey : [''],
      uploadKey : [''],
      uploadKeyHidden : [''],
      retypeUploadKey : [''],
      userEncoding : [''],
      cvvKeyA : [''],
      cvvKeyB : [''],
      bankKeyModule : [''],
      arKeyModule : [''],
      processorName : [''],
      subProcessorName : [''],
      processorData : [''],
      processorInfo : [''],
      CVVKEYIND : [''],
      DoNotPromptBehavior : [''],
      enrollmentLocales : [''],
      userIdEnabled : [''],
      twoStepLoginEnabled : [''],
      dualAuthRequiredAtCommit : [''],
      collectAdminContactDetails : [''],
      slt_Admins : [''],
      slt_selAdmins : [''],
    })
   }

  ngOnInit() {
    this.getTimeZoneList().subscribe(data =>{
      this.timeZones = data;    
    });
    this.fetchAllCountries();
    this.fetchAllLocales();
    this.fetchAllAdmins();
  }

  getUnique(array) {
    var uniqueArray = [];
    for (let i = 0; i < array.length; i++) {
      if (uniqueArray.indexOf(array[i]) === -1) {
        uniqueArray.push(array[i]);
      }
    }
    return uniqueArray;
  }
  addAdmins() {
    for (let i = 0; i < this.createIssuerForm.get('slt_Admins').value.length; i++) {
      this.selectedAdmins.push(this.createIssuerForm.get('slt_Admins').value[i]);
    }
    this.selectedAdmins = this.getUnique(this.selectedAdmins);
  }

  removeAdmins() {
    this.selectedRemoveAdmins.length = 0;
    for (let i = 0; i < this.createIssuerForm.get('slt_selAdmins').value.length; i++) {
      this.selectedRemoveAdmins.push(this.createIssuerForm.get('slt_selAdmins').value[i]);
    }
    for (let j = 0; j < this.selectedRemoveAdmins.length; j++) {
      this.selectedAdmins.splice($.inArray(this.selectedRemoveAdmins[j], this.selectedAdmins), 1)
    }
  }

  public getTimeZoneList(): Observable<any> {
    return this.http.get("../assets/constants/timeZoneList.json");
  }


  fetchAllCountries(){
    this.service.getAllCountries().subscribe((countriresData) => {
      this.countries = countriresData.response;
    })
  }

  fetchAllLocales(){
    this.service.getAllLocales().subscribe((localesData) => {
      this.locales = localesData.response;
    })
  }

  fetchAllAdmins(){
    this.service.getAllAdmins().subscribe((adminData) => {
      this.admins = adminData.response;
    })
  }
//   createIssuers(){
//     this.service.createIssuer(this.createIssuerForm.value);
//   //   this.service.createIssuer(this.createIssuerForm.value).subscribe((response) => {
//   //     console.log(response);
      
//   //     if(response.statusCode === 1){
//   //       this.errorMessage = "Issuer "+ response.bankName +" Added"
//   //     }
//   //     else {
//   //       this.errorMessage = "someErrors"
//   //     }
//   //   })  
//   // }
//  }
 createIssuers(){
  this.service.createIssuer(this.createIssuerForm.value);
}
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UpdateFiInformationService } from '../../../../Services/issuer_configuration_service/update_FI_information_service/update-fi-information.service';

@Component({
  selector: 'app-update-fi-information',
  templateUrl: './update-fi-information.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class UpdateFiInformationComponent implements OnInit {

  updateFIinformationForm : FormGroup;

  constructor( private formBuilder : FormBuilder, private service : UpdateFiInformationService ) { 
    this.updateFIinformationForm = this.formBuilder.group({
      bankName : [''],
      CardRangeName : [''],
      cardRangeGroup : [''],
      fiBusinessID : [''],
      fiBIN : [''],
      cardType : [''],
      panLength : [''],
      beginRange : [''],
      endRange : [''],
      termPolicyVersion1 : [''],
      termPolicyVersion2 : [''],
      mobileEnabled : [''],
      signingKeyModule : [''],
      authResultKeyModule : [''],
      chipKeyModule : [''],
      AAVAlgorithm : [''],
      mcKeyID : [''],
      mcKeyAlias : [''],
      brandingURL1 : [''],
      brandingURL2 : [''],
      acsURL1 : [''],
      acsURL2 : [''],
      acsURL3 : [''],
      acsURL4 : [''],
      acsURL5 : [''],
      CAPURLParameters : [''],
      signingCertFile : [''],
      ahsReceiptURL : [''],
      authSelect : [''],
      authFallback : [''],
      maxAuthTries : ['3'],
      authPriority : [''],
      nStrikesAcrossSessions : [''],
      maxAuthTriesForFYP : [''],
      authMethod : [''],
      pluginURL : [''],
      eAccess : [''],
      pluginName : [''],
      hsmVariant : [''],
      pluginVersion : [''],
      cvvKeyA : [''],
      cvvKeyB : [''],
      CVVKEYIND : [''],
      passwordChangeVelocity : ['0'],
      enrollOption : [''],
      ActualEnrollOption : [''],
      ActualMaxWelcome : [''],
      ActualMaxDecline : [''],
      backupattempts : [''],
      maxDeclines : [''],
      velogging : [''],
      maxWelcome : [''],
      EnableAttemptsAfterMaxDeclines : [''],
      arcotOtpEnabled : ['']
    })
   }

  ngOnInit() {
  }

  updateFIinformation(){
    this.service.updateFIinformation(this.updateFIinformationForm.value).subscribe((response) =>{
      console.log(response);
    })
  }

}

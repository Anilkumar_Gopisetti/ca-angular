import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FiInformationService } from '../../../../Services/issuer_configuration_service/FI_information_services/fi-information.service';

@Component({
  selector: 'app-add-fi-information',
  templateUrl: './add-fi-information.component.html',
  styleUrls: ['../../../../../assets/css/forms.css']
})
export class AddFIInformationComponent implements OnInit {
  
  addFIinformationForm : FormGroup;
  issuerBanks : any;
  cardRanges : any;
  errorMessage : string;
  constructor( private formBuilder : FormBuilder, private service : FiInformationService) { 
    this.addFIinformationForm = this.formBuilder.group({
      bankName : [''],
      CardRangeName : [''],
      cardRangeGroup : [''],
      fiBusinessID : [''],
      fiBIN : [''],
      cardType : [''],
      panLength : [''],
      beginRange : [''],
      endRange : [''],
      termPolicyVersion1 : [''],
      termPolicyVersion2 : [''],
      mobileEnabled : [''],
      signingKeyModule : [''],
      authResultKeyModule : [''],
      chipKeyModule : [''],
      AAVAlgorithm : [''],
      mcKeyID : [''],
      mcKeyAlias : [''],
      brandingURL1 : [''],
      brandingURL2 : [''],
      acsURL1 : [''],
      acsURL2 : [''],
      acsURL3 : [''],
      acsURL4 : [''],
      acsURL5 : [''],
      CAPURLParameters : [''],
      signingCertFile : [''],
      ahsReceiptURL : [''],
      authSelect : [''],
      authFallback : [''],
      maxAuthTries : [''],
      authPriority : [''],
      nStrikesAcrossSessions : [''],
      maxAuthTriesForFYP : [''],
      authMethod : [''],
      pluginURL : [''],
      eAccess : [''],
      pluginName : [''],
      hsmVariant : [''],
      pluginVersion : [''],
      cvvKeyA : [''],
      cvvKeyB : [''],
      CVVKEYIND : [''],
      passwordChangeVelocity : [''],
      enrollOption : [''],
      ActualEnrollOption : [''],
      ActualMaxWelcome : [''],
      ActualMaxDecline : [''],
      backupattempts : [''],
      maxDeclines : [''],
      velogging : [''],
      maxWelcome : [''],
      EnableAttemptsAfterMaxDeclines : [''],
    });
   }

  ngOnInit() {
    this.receiveAllIssuerBanks();
    this.receiveCardRangeGroups();
  }

receiveAllIssuerBanks(){
    this.service.getAllIssuerBanks().subscribe((data) => {
      this.issuerBanks = data.response;
      console.log(this.issuerBanks);
    })
  }

receiveCardRangeGroups(){
  this.service.getAllCardRangeGroup().subscribe((data) => {
    this.cardRanges = data.response;
    console.log(this.cardRanges);
  })
}

addFIInformation(){
  this.service.createCardRangeForIssuer(this.addFIinformationForm.value).subscribe((data)=>{
    if(data.statusCode === 1){
      this.errorMessage = "information Added successfully";
    }
    else{
    this.errorMessage = data.response;
    }
  })
}

}

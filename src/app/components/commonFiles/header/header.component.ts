import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../../../Services/auth_service/auth-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private service: AuthServiceService) { }
  locales:any=[];
  
  ngOnInit() {
    this.service.getLocales().subscribe((data: any) => {
      this.locales = data.response;
      })
  }


  doLogout(){
    localStorage.removeItem('token');
    localStorage.removeItem('sessionData');
    localStorage.removeItem('entered');
    localStorage.removeItem('path');
    localStorage.removeItem('username');
    localStorage.removeItem('password');
    localStorage.removeItem('redirectpage')
    window.location.href = "/";
  }
}

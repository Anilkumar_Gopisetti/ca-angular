import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UpdateProfileService } from '../../../Services/update_profile_service/update-profile.service';
import { AuthServiceService } from '../../../Services/auth_service/auth-service.service';
@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['../../../../assets/css/forms.css']
})
export class UpdateProfileComponent implements OnInit {
  updateForm: FormGroup
  currentURL: any = this.router.url;
  showReports: boolean;
  errorMessage: string;
  timeZones: Object;
  adminData : any;
  locales : any;
  timeStampList : any = ["yyyy-MM-dd hh:mm:ss a z" , "MM-dd-yyyy hh:mm:ss a z", "dd-MM-yyyy hh:mm:ss a z", "yyyy/MM/dd hh:mm:ss a z", "MM/dd/yyyy hh:mm:ss a z", "dd/MM/yyyy hh:mm:ss a z", "dd-MM-yyyy HH:mm:ss z", "dd/MM/yyyy HH:mm:ss z"] 
  constructor(private router: Router, private formbuilder: FormBuilder,
    private service:UpdateProfileService, private localeservice: AuthServiceService) {
    this.updateForm = this.formbuilder.group({
      jobTitle: [''],
      supervisorName: [''],
      adminEmailAddress: [''],
      adminPhoneNumber: [''],
      currentpassword: [''],
      adminHintQuestion: [''],
      adminHintAnswer1: [''],
      adminHintAnswer2: [''],
      recordsPerPage: [''],
      startdate: [''],
      startDateOffset: [''],
      adminLocale: [''],
      localTimeZone: [''],
      timeStampFormat: ['']
    })
  }

  ngOnInit() {
    this.checkRouterLink();
  }

  fetchTimeZones(){
    this.service.getAllTimeZones().subscribe(listOfTimeZones => {
      this.timeZones = listOfTimeZones;
      console.log(this.timeZones);
    })
  }

  fetchLocales(){
    this.localeservice.getLocales().subscribe((data: any) => {
      this.locales = data.response;
      })
  }
  checkRouterLink() {
    if (this.currentURL == "/update_profile/first_login") {
      this.showReports = false;
      this.errorMessage = "Update Profile At Login";
    }
    else if (this.currentURL == "/update_profile") {
      this.showReports = true;
      this.errorMessage = "";
      // this.fetchLocales();
      this.fetchTimeZones();
      this.fetchProfileDetails();
    }
  }

  fetchProfileDetails(){
    debugger;
    this.service.getProfileData().subscribe((data) => {
      this.adminData = data.response;
      this.updateForm.get('adminEmailAddress').patchValue(this.adminData.EMAILADDRESS);
      this.updateForm.get('jobTitle').patchValue(this.adminData.JOBTITLE);
      this.updateForm.get('supervisorName').patchValue(this.adminData.SUPERVISORNAME);
      this.updateForm.get('adminHintQuestion').patchValue(this.adminData.HINTQUESTION);
      this.updateForm.get('adminHintAnswer1').patchValue(this.adminData.HINTANSWER);
      this.updateForm.get('adminHintAnswer2').patchValue(this.adminData.HINTANSWER);

      this.updateForm.get('adminPhoneNumber').patchValue(this.adminData.PHONEMUMBER);
      this.updateForm.controls['recordsPerPage'].setValue(this.adminData.RECNUMPERPAGE , { onlySelf: true });
      this.updateForm.controls['startDateOffset'].setValue(this.adminData.startdateoffset , { onlySelf: true });
      this.updateForm.controls['adminLocale'].setValue("en_US" , { onlySelf: true });
      this.updateForm.controls['localTimeZone'].setValue(this.adminData.REPORTTIMEZONELOCAL , { onlySelf: true });
      this.updateForm.controls['timeStampFormat'].setValue(this.adminData.REPORTTIMESTAMPFORMAT , { onlySelf: true });  
    })
  }

  doCancel(){
    this.router.navigate(['/index']);
  }

  updateProfileAtlogin() {
    sessionStorage.setItem('update', 'updatePage')
    this.service.updateAdminProfile(this.updateForm.value).subscribe((data)=>{
    console.log(data);  
    if(data.statusCode == 1){
      // sessionStorage.setItem('updated', 'updated') 
      // window.location.href = "/index";
      this.errorMessage = data.message;
    }
    else{
      this.errorMessage = data.message;
    }
    })
  }
}

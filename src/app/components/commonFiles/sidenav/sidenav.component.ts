import { Component, OnInit } from '@angular/core';
import { ErrorMessagesService } from '../../../Services/service_for_errorMessages/error-messages.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  expanded: boolean = true;
  navData: any = [];

  constructor(private service: ErrorMessagesService) {
    
  }

  ngOnInit() {
    this.service.getNavbarDetails().subscribe((data) => {
      this.navData = data;
    });
  }

  imageChange(value) {
    var URL = value.src;
    var newURL = URL.replace(/^[a-z]{4,5}\:\/{2}[a-z]{1,}\:[0-9]{1,4}.(.*)/, '$1'); // http or https
    if (newURL == "assets/minus.gif") {
      value.src = "../../assets/plus.gif";
    }
    else {
      value.src = "../../assets/minus.gif";
    }
  }
}

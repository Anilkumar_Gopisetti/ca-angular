import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthServiceService } from '../../../Services/auth_service/auth-service.service';
import { ErrorMessagesService } from '../../../Services/service_for_errorMessages/error-messages.service';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['../../../../assets/css/forms.css']
})

export class ChangePasswordComponent implements OnInit {
  errorMessage: string = "Change Password At First Login";
  changePassword: FormGroup;
  errorData: any;
  isEntered: boolean = false
  constructor(private formBuilder: FormBuilder, private service: AuthServiceService, private errorService: ErrorMessagesService) {
    this.errorService.getJSON().subscribe((data) => {
      this.errorData = data;
    });


    this.changePassword = this.formBuilder.group({
      oldPassword:[''],
      password: [''],
      retypeNewpassword: [''],
    });
  }
  ngOnInit() {
    if (localStorage.getItem("entered") == "true") {
      this.isEntered=true
    }
  }

  doCancel() {
    window.location.href = "/";
  }

  dochangePassword() {
    this.service.dochangePassword(this.changePassword.value).subscribe((response) => {
      console.log(response.response);
      this.errorMessage = response.response;
      if (response.statusCode == 1) {
        if(localStorage.getItem('entered')=="false"){
          window.location.href = "/index";
        }
       else{
         this.errorMessage= response.response
       }
      }
      else if (response.statusCode == 3) {
        window.location.href = "/update_profile/first_login";
      }
      else {
        for (let i = 0; i < this.errorData.length; i++) {
          if (this.errorData[i].errorCode == response.response) {
            this.errorMessage = this.errorData[i].errorMessage;
            break;
          }
        }
      }
    })
  }

}
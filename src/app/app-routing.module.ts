import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginpageComponent } from './components/Global Admin/loginpage/loginpage.component';
import { MasterAdminLoginComponent } from './components/Master Admin/master-admin-login/master-admin-login.component';
import { IndexComponent } from './components/commonFiles/index/index.component';
import { ChangePasswordComponent } from './components/commonFiles/change-password/change-password.component';
import { UpdateProfileComponent } from './components/commonFiles/update-profile/update-profile.component';
import { HelpComponent } from './components/commonFiles/help/help.component';
import { CreateIssuerComponent } from './components/Global Admin/Issuer Configurations/create-issuer/create-issuer.component';
import { AddFIInformationComponent } from './components/Global Admin/Issuer Configurations/add-fi-information/add-fi-information.component';
import { UpdateFiInformationComponent } from './components/Global Admin/Issuer Configurations/update-fi-information/update-fi-information.component';
import { AddIssuerCustomizationComponent } from './components/Global Admin/Issuer Configurations/add-issuer-customization/add-issuer-customization.component';
import { UpdateIssuerComponent } from './components/Global Admin/Issuer Configurations/update-issuer/update-issuer.component';
import { UploadSigningCertificateComponent } from './components/Global Admin/Issuer Configurations/upload-signing-certificate/upload-signing-certificate.component';
import { EnableCardRangesComponent } from './components/Global Admin/Issuer Configurations/enable-card-ranges/enable-card-ranges.component';
import { CreateGlobalAdminComponent } from './components/Global Admin/Global admin configurations/create-global-admin/create-global-admin.component';
import { ModifyGlobalAdminComponent } from './components/Global Admin/Global admin configurations/modify-global-admin/modify-global-admin.component';
import { ResetGlobalAdminComponent } from './components/Global Admin/Global admin configurations/reset-global-admin/reset-global-admin.component';
import { UpdateGlobalAdminComponent } from './components/Global Admin/Global admin configurations/update-global-admin/update-global-admin.component';
import { CreateIssuerAdminComponent } from './components/Global Admin/Issuer Admin Configurations/create-issuer-admin/create-issuer-admin.component';
import { ConfigureIssuerAdminPolicyComponent } from './components/Global Admin/Issuer Admin Configurations/configure-issuer-admin-policy/configure-issuer-admin-policy.component';
import { ModifyIssuerAdminAccountComponent } from './components/Global Admin/Issuer Admin Configurations/modify-issuer-admin-account/modify-issuer-admin-account.component';
import { ResetIssuerAdminPasswordComponent } from './components/Global Admin/Issuer Admin Configurations/reset-issuer-admin-password/reset-issuer-admin-password.component';
import { UpdateIssuerAdminPrevilegesComponent } from './components/Global Admin/Issuer Admin Configurations/update-issuer-admin-previleges/update-issuer-admin-previleges.component';
import { ConfigureIssuerAdminPrevilegesComponent } from './components/Global Admin/Issuer Admin Configurations/configure-issuer-admin-previleges/configure-issuer-admin-previleges.component';
import { ViewAndUpdateCaseComponent } from './components/Global Admin/case management/view-and-update-case/view-and-update-case.component';
import { ConfigureIssuerParametersComponent } from './components/Global Admin/Issuer Configurations/configure-issuer-parameters/configure-issuer-parameters.component';
import { FormInfoComponent } from './components/commonFiles/form-info/form-info.component';



const routes: Routes = [
  { path: '', component: LoginpageComponent  },
  { path: 'masterAdminLogin', component: MasterAdminLoginComponent },
  { path: 'index', component: IndexComponent },
  { path: 'change_password', component: ChangePasswordComponent },
  // { path: 'update_profile', component: UpdateProfileComponent },
  { path: 'update_profile/first_login', component: UpdateProfileComponent },
  { path: 'help', component: HelpComponent },
  { path: 'isco/create_issuer', component: CreateIssuerComponent },
  { path: 'isco/add_FI_information', component: AddFIInformationComponent },
  { path: 'isco/update_FI_information', component: UpdateFiInformationComponent },
  { path: 'isco/add_issuer_customization', component: AddIssuerCustomizationComponent },
  { path: 'isco/configure_issuer_parameters', component: ConfigureIssuerParametersComponent },
  { path: 'isco/update_issuer', component: UpdateIssuerComponent },
  { path: 'isco/upload_signing_certificate', component: UploadSigningCertificateComponent },
  { path: 'isco/enable_card_ranges', component: EnableCardRangesComponent },
  { path: 'gaco/global_admin/create', component: CreateGlobalAdminComponent },
  { path: 'gaco/global_admin/modify', component: ModifyGlobalAdminComponent },
  { path: 'gaco/global_admin/reset', component: ResetGlobalAdminComponent },
  { path: 'gaco/global_admin/update', component: UpdateGlobalAdminComponent },
  { path: 'iaco/issuer_admin/create', component: CreateIssuerAdminComponent },
  { path: 'iaco/issuer_admin/configure_policy', component: ConfigureIssuerAdminPolicyComponent },
  { path: 'iaco/issuer_admin/modify', component: ModifyIssuerAdminAccountComponent },
  { path: 'iaco/issuer_admin/reset_password', component: ResetIssuerAdminPasswordComponent },
  { path: 'iaco/issuer_admin/update_privilages', component: UpdateIssuerAdminPrevilegesComponent },
  { path: 'iaco/issuer_admin/configure_privilages', component: ConfigureIssuerAdminPrevilegesComponent },
  { path: 'cmgt/view_and_update_case', component: ViewAndUpdateCaseComponent },
  { path: 'cmgt/view_and_update_case', component: ViewAndUpdateCaseComponent },
  { path: 'formInfo', component: FormInfoComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}

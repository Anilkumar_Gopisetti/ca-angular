import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ConfigureIssuerAdminPolicyService {

  url: string = "http://192.168.150.111:8080/vpas/api/issueradmin/policy/configure";
  bankListUrl :string = "http://192.168.150.111:8080/vpas/api/admin/banklist";
  sessionData: any = JSON.parse(localStorage.getItem("sessionData"));

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': this.sessionData.token
    })
  }

  // bankoptions = {
  //   headers: new HttpHeaders({
  //     'adminName': 'GLOBAL',
  //     'secAdminName': 'GLOBAL'
  //   })
  // }

  constructor(private http: HttpClient) { }
  configureIssuer(data) {
    let allowPasswordReset = '0';
    if (data.AllowPasswordReset) {
      allowPasswordReset = '1';
    }
    let body = {
      "loggedInAdminName": this.sessionData.adminName,
      "locale": "en_US",
      "loggedInBank": "i18n/en_US",
      // "adminNameForUpdate": data.issuer,
      "adminNameForUpdate": "fSNBKDmUNKojkN8dAFBKkr7G6AD4rLSq",
      "level": "3",
      "selectedBank": data.issuer,
      "action": "submit",
      "maxTriesPerSession": data.MaxTriesPerSession,
      "maxTriesAcrossSession": data.MaxTriesAcrossSession,
      "maxLength": data.MaxLength,
      "minLength": data.MinLength,
      "minAlphabet": data.MinAlphabet,
      "minNumeric": data.MinNumeric,
      "minSpecialCharacters": data.MinSpecialCharacters,
      "minLowerCaseCharacters": data.MinLowerCaseCharacters,
      "minUpperCaseCharacters": data.MinUpperCaseCharacters,
      "passwordRenewalFrequency": data.PasswordRenewalFrequency,
      "maxInactivityPeriod": data.MaxInactivityPeriod,
      "allowPasswordReset": allowPasswordReset,
      "passwordStorageAlgorithm": data.passwordStorageAlgorithm,
      "passwordChangeVelocity": data.passwordChangeVelocity,
      "loginSessionTimeout": data.loginSessionTimeout,
      "collectAdminContactDetails": "2"
    }
    console.log(body)
    return this.http.post<any>(this.url, body, this.httpoptions);
  }

  getAllIssuers() {
    return this.http.get<any>(this.bankListUrl, this.httpoptions)
  }

}

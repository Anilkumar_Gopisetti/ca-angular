import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ConfigureIssuerAdminPrivilegesService {

  privilegeUrl = "http://192.168.150.111:8080/vpas/api/issueradmin/privileges";
  updatePrivilegesUrl = "http://192.168.150.111:8080/vpas/api/issueradmin/privileges/configure"
  sessionData: any = JSON.parse(localStorage.getItem("sessionData"));
  token: any = this.sessionData.token;
  constructor(private http: HttpClient) { }
  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': this.token,
    })
  }
  getAllIssuerPrivilegeData() {
    return this.http.get<any>(this.privilegeUrl,this.httpoptions);
  }

  updatePrivileges(privIds,privType,privNumberOfAdmins,privEnable) {
    let privilegeType= [];
    for(let i=0;i<privType.length;i++)
    {
      privilegeType.push(privType[i].toString());
    }

   let body = {
      "level": "3",
      "action": "updatePrivileges",
      "privType": privilegeType,
      "privIds": privIds,
      "privNumberOfAdmins": privNumberOfAdmins,
      "privEnable": privEnable,
      "locale": "en_US",
      "bankId": "0"
    }
    console.log(body);
    return this.http.put<any>(this.updatePrivilegesUrl, body ,this.httpoptions)
  }
}

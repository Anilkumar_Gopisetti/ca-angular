import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ResetIssuerAdminPasswordService {
  userUrl: string = "http://192.168.150.111:8080/vpas/api/admin/";
  bankUrl: string = "http://192.168.150.111:8080/vpas/api/admin/banklist"
  resetUrl: string = "http://192.168.150.111:8080/vpas/api/admin/reset/password"
  sessionData: any = JSON.parse(localStorage.getItem("sessionData"));
  token: any = this.sessionData.token;
  constructor(private http: HttpClient) { }
  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': this.token,
    })
  }

  getAllAdmins() {
    return this.http.get<any>(this.bankUrl, this.httpoptions)
  }
  getAllUsers(data) {
    return this.http.get<any>(this.userUrl + 'list/' + data, this.httpoptions)
  }
  resetIssuerAdmin(data) {
    let body = {
      "loggedInAdminName": this.sessionData.adminName,
      "loggedInUserid": "global",
      "selectedBank": data.issuer,
      "selectedAdmin": data.Admins,
      "password": data.newPassword,
      "remarks":data.remark,
      "level": 3,
      "action": "updateUser",
      "reCheckPassword": data.retypeNewpassword,
      "changePwdAtLogin": "true",
      "emailCheck": "on",
      "phoneCheck": "on",
      "locale": "en_US",
      "loggedInBank": "i18n/en_US",
      "adminNameForUpdate": "fSNBKDmUNKojkN8dAFBKkr7G6AD4rLSq"
    }
    console.log(body);
    return this.http.put<any>(this.resetUrl, body, this.httpoptions)
  }
}

import { TestBed } from '@angular/core/testing';

import { ResetIssuerAdminPasswordService } from './reset-issuer-admin-password.service';

describe('ResetIssuerAdminPasswordService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResetIssuerAdminPasswordService = TestBed.get(ResetIssuerAdminPasswordService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class UpdateIssuerAdminPrivilegesService {

  url: string = "http://192.168.150.111:8080/vpas/api/admin/"
  sessionData: any = JSON.parse(localStorage.getItem("sessionData"));
  token: any = this.sessionData.token;
  constructor(private http: HttpClient) { }
  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': this.token,
    })
  }

  fetchAllBanks() {
    return this.http.get<any>(this.url + "banklist", this.httpoptions)
  }
  fetchAllAdmins(data) {
    console.log(data);
    return this.http.get<any>(this.url + "list/" + data, this.httpoptions)
  }
  fetchAdminDetails(data) {
    let body = {
      "locale": "en_US",
      "action": "changeAdmin",
      "adminIdClear": data.selectedAdminName,
      "adminBankId": data.selectedBankId,
      "level": "3",
      "bank": "i18n/en_US"
    }
    return this.http.put<any>(this.url + "privilege", body, this.httpoptions)
  }
  updateIssuerAdmin(data, bankId, adminName) {
    console.log(data);
    let  array = [0,1,2,3,4,5,6];
    let body = {
      "locale": "en_US",
      "action": "saveAdmin",
      "adminIdClear": adminName,
      "adminBankId": bankId,
      "firstNameClear": data.firstNameClear,
      "middleNameClear": data.middleNameClear,
      "lastNameClear": data.lastNameClear,
      "description": data.description,
      "selectedBanks": ["23"],
      "blockkSimultaneousLogin": data.blockkSimultaneousLogin,
      "updateProfileAtLogin": data.updateProfileAtLogin,
      "maxValForLastPwdList": data.maxValForLastPwdList,
      "passwordType": true,
      "subProcessorName": "-1000",
      "processorName": "-1000",
      "twoFactorAuthenticationType": 1,
      "twoFactorDeliveryChannelType": data.twoFactorDeliveryChannelType,
      "twoFactorAuthenticationEnabled": true,
      "collectAdminContactDetails":data.collectAdminContactDetails,
      "passwordTypeChanged": true,
      "level": "3",
      "pwdNeverExpires": data.pwdNeverExpires,
      "bank": "i18n/en_US",
      "privileges": data.privs,
      "templateIdClear": data.templateIdClear
    }
    return this.http.put<any>(this.url + "privilege", body, this.httpoptions)
  }
}

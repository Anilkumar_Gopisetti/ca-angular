import { TestBed } from '@angular/core/testing';

import { UpdateIssuerAdminPrivilegesService } from './update-issuer-admin-privileges.service';

describe('UpdateIssuerAdminPrivilegesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UpdateIssuerAdminPrivilegesService = TestBed.get(UpdateIssuerAdminPrivilegesService);
    expect(service).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';

import { ModifyIssuerAdminService } from './modify-issuer-admin.service';

describe('ModifyIssuerAdminService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModifyIssuerAdminService = TestBed.get(ModifyIssuerAdminService);
    expect(service).toBeTruthy();
  });
});

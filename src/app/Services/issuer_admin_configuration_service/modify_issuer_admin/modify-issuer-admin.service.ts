import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ModifyIssuerAdminService {
  
// url:string="http://192.168.150.111:8080/vpas/issueradmin/"
url:string="http://192.168.150.111:8080/vpas/api/admin/"
bankUrl :string = "http://192.168.150.111:8080/vpas/api/admin/banklist";
sessionData: any = JSON.parse(localStorage.getItem("sessionData"));
token: any = this.sessionData.token;
constructor(private http: HttpClient) { }
httpoptions = {
  headers: new HttpHeaders({
    'Authorization': this.token,
    'Content-Type': "application/json"
  })
}
getAllAdmins(){
  return this.http.get<any>(this.bankUrl,this.httpoptions);
}
getAllUsers(data){
  return this.http.get<any>(this.url+"list/"+data, this.httpoptions);
}

getAdminDetails(adminData){
  console.log(adminData);
  return this.http.get<any>(this.url + 'status/' + adminData.adminName + "/" + adminData.bankId, this.httpoptions)
}

modifyGlobalAdmin(data) {
  let body = {
    "selectedAdminName":data.Admins,
    "selectedAdminBank": data.issuer,
    "remark": data.remark,
    "level": "3",
    "action": "updateUser",
    "status": data.status,
    "locale": "en_US",
    "bank": "i18n/en_US",
    "adminForUpdate": data.Admins

  }
  return this.http.put<any>(this.url+"changestatus", body, this.httpoptions)
}

}

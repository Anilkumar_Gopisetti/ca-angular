import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class CreateIssuerAdminService {

  url : string = "http://192.168.150.111:8080/vpas/api/issueradmin/";
  sessionData: any = JSON.parse(localStorage.getItem("sessionData"));

  constructor(private http : HttpClient) { }
  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': this.sessionData.token,
    })
  }

getIssuerPrivilegeList(){
  return this.http.get<any>(this.url + 'privileges/3', this.httpoptions);
}

createIssuer(formData){

  {
    if (formData.changePasswordAtFirstLogin == true) {
      formData.changePasswordAtFirstLogin = "true";
    }
    else {
      formData.changePasswordAtFirstLogin = "false"
    }
  }
  {
    if (formData.pwdNeverExpires == true) {
      formData.pwdNeverExpires = "1";
    }
    else {
      formData.pwdNeverExpires = "0"
    }
  }
  {
    if (formData.blkSimultLogin == true) {
      formData.blockksimultlogin = "1";
    }
    else {
      formData.blockksimultlogin = "0"
    }

  }
  {
    if (formData.updateProfileAtLogin == true) {
      formData.updateProfileAtLogin = "1";
    }
    else {
      formData.updateProfileAtLogin = "0";
    }
  }

  const body = {
    "adminUserId":formData.userid,
    "firstName": formData.firstName,
    "lastName": formData.lastName,
    "level":3,
    "middleName": formData.middleName,
    "desciption": formData.description,
    "maxValForLastPwdList": formData.maxValForLastPwdList,
    "pasword": formData.password,
    "rePassword": formData.rePassword,
    "changePasswordAtFirstLogin": formData.changePasswordAtFirstLogin,
    "pwdNeverExpires": formData.pwdNeverExpires,
    "updateProfileAtLogin": formData.updateProfileAtLogin,
    "collectAdminContactDetails": "0",
    "twoFactorDeliveryChannelType": "0",
    "countries" : null,
    // "subProcessorName":null,
    "selectedAdminBank":formData.issuer,
    "selectedTemplateName":formData.Admins,
    "secAdminName":"global",
    "secAdminName_decrypted":"global",
    "multiControl":"false",
    "blkSimultLogin":formData.blockksimultlogin,
    // "blksimultLogin":formData.blockksimultlogin,
    "is2FAAuthenticationEnabled":"0",  
    "twoFactorDelChnlType1":"0",
    "twoFactorAuthenticationType":formData.twoFactorAuthenticationType,
    "deliveryChannel":"0",
    "privs":formData.privs,
    "bank":"i18n/en_US",
    "action":"newUser"
    }
  console.log(body);
  return this.http.post<any>(this.url, body, this.httpoptions); 
}

}

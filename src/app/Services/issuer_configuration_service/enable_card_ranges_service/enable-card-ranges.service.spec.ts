import { TestBed } from '@angular/core/testing';

import { EnableCardRangesService } from './enable-card-ranges.service';

describe('EnableCardRangesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EnableCardRangesService = TestBed.get(EnableCardRangesService);
    expect(service).toBeTruthy();
  });
});

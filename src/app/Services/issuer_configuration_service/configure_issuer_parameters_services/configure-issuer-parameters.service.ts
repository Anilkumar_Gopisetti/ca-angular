import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class ConfigureIssuerParametersService {

  sessionData: any = JSON.parse(localStorage.getItem("sessionData"));
  token: any = this.sessionData.token;

  constructor(private http: HttpClient) {

  }

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': this.token,
      'Content-Type': "application/json"
    })
  }

  getIssuers() {
    let body = {
      "privId": "S6305",
      "actualPage": "EditIssuerParameters.jsp",
      "actionUrl": "EditIssuerParameters.jsp",
      "bank": "i18n/en_US",
      "level": "",
      "locale": "en_US",
      "loggedinlevel": "2",
      "auth": "1",
      "adminName": "XHEC18tn55UAPzKtu9cV6Q==",
      "adminLoggedInBank": "i18n/en_US",
      "acl_decrypted": "S9200:S9201:S8350:S9202:S9203:S6210:S6211:IDS.ACCT.D:S6450:S7023:IDS.TDSTOKDETOK:S3904:S6214:S6215:S6212:S7026:S9204:S6218:S9601:S9205:S6216:S6217:IDS.LINK.W:S9312:S9313:S6442:S7011:S8188:S12154:S13001:S12155:S13002:S6209:S8909:S12153:IDS.TOKDETOK:S6204:S6201:S8985:S6202:S6207:S6208:S6205:S6206:S6470:S8492:S6232:S6233:S6230:S6990:S6231:S12143:S12144:FM.ENROLL:IDS.ENCDEC:S6994:S6999:S3403:S3402:S9900:S6997:S9851:S8400:S6221:S6101:S6222:S3471:S6220:IDS.ACCT.W:FM.AUTH:IDS.ACCT.R:S11200:S6225:S6501:S6226:S6984:S6102:S6223:S6983:S6103:S6224:S6982:S6229:S6989:S6988:S8923:S6227:S6228:S9882:S9240:S84921:S9881:S12115:FM.RISK:S6418:S3549:S6419:S6412:S9923:S8557:S6413:S6410:S6411:S6416:S6417:S6414:S8559:S9992:S9112:S8541:S9111:S6409:S6408:S8943:S6401:S6402:S3495:S6405:S9997:S6406:S6403:S6404:S9300:IDS.FACT.R:S6310:S6431:IDS.FACT.W:S12101:S6314:S6311:S6432:S6433:S6438:S6439:S6315:S6436:S6316:S6437:S9134:S9135:S6420:IRIS.IDSCONFIG.TOOL:S6308:S6429:S6309:S6302:S8327:S6303:S6421:S6301:S6427:S6428:IDS.FACT.D:S9810:S6304:S6305",
      "nameDecrypted": "GLOBAL",
      "nameEncrypted": "XHEC18tn55UAPzKtu9cV6Q==",
      "authorizedBanksBitMap": "f39/f39/f39/f39/fwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=",
      "contextPath": "/vpas",
      "requestURI": "/vpas/admin/getBank.jsp",
      "queryString": "privId=S6305&actualPage=EditIssuerParameters.jsp&actionUrl=EditIssuerParameters.jsp&bank=i18n/en_US&locale=en_US&loggedinlevel=2&auth=1"
    }
    console.log(body);
    return this.http.post<any>("http://localhost:8080/vpas/api/issuer/getBankDetails", body, this.httpoptions)
  }
  getIssuerParams() {
    let body = {
      "privId": "S6305",
      "actualPage": "EditIssuerParameters.jsp",
      "actionUrl": "EditIssuerParameters.jsp",
      "bank": "i18n/en_US",
      "level": "",
      "locale": "en_US",
      "loggedinlevel": "2",
      "auth": "1",
      "adminName": "XHEC18tn55UAPzKtu9cV6Q==",
      "adminLoggedInBank": "i18n/en_US",
      "acl_decrypted": "S9200:S9201:S8350:S9202:S9203:S6210:S6211:IDS.ACCT.D:S6450:S7023:IDS.TDSTOKDETOK:S3904:S6214:S6215:S6212:S7026:S9204:S6218:S9601:S9205:S6216:S6217:IDS.LINK.W:S9312:S9313:S6442:S7011:S8188:S12154:S13001:S12155:S13002:S6209:S8909:S12153:IDS.TOKDETOK:S6204:S6201:S8985:S6202:S6207:S6208:S6205:S6206:S6470:S8492:S6232:S6233:S6230:S6990:S6231:S12143:S12144:FM.ENROLL:IDS.ENCDEC:S6994:S6999:S3403:S3402:S9900:S6997:S9851:S8400:S6221:S6101:S6222:S3471:S6220:IDS.ACCT.W:FM.AUTH:IDS.ACCT.R:S11200:S6225:S6501:S6226:S6984:S6102:S6223:S6983:S6103:S6224:S6982:S6229:S6989:S6988:S8923:S6227:S6228:S9882:S9240:S84921:S9881:S12115:FM.RISK:S6418:S3549:S6419:S6412:S9923:S8557:S6413:S6410:S6411:S6416:S6417:S6414:S8559:S9992:S9112:S8541:S9111:S6409:S6408:S8943:S6401:S6402:S3495:S6405:S9997:S6406:S6403:S6404:S9300:IDS.FACT.R:S6310:S6431:IDS.FACT.W:S12101:S6314:S6311:S6432:S6433:S6438:S6439:S6315:S6436:S6316:S6437:S9134:S9135:S6420:IRIS.IDSCONFIG.TOOL:S6308:S6429:S6309:S6302:S8327:S6303:S6421:S6301:S6427:S6428:IDS.FACT.D:S9810:S6304:S6305",
      "nameDecrypted": "GLOBAL",
      "nameEncrypted": "XHEC18tn55UAPzKtu9cV6Q==",
      "authorizedBanksBitMap": "f39/f39/f39/f39/fwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=",
      "contextPath": "/vpas",
      "requestURI": "/vpas/admin/EditIssuerParameters.jsp",
      "queryString": "bank=i18n/en_US&locale=en_US&loggedinlevel=2&actionUrl=EditIssuerParameters.jsp&privId=S6305",
      "secondaryBank": "100"
    }
    console.log(body);
    return this.http.post<any>("http://localhost:8080/vpas/api/issuer/getIssuerParamDetails", body, this.httpoptions)
  }

  submitEditParams()
  {
    let body =
      {
        "bank": "i18n/en_US",
        "bankId" : 91,
        "adminName": "XHEC18tn55UAPzKtu9cV6Q==",
        "locale": "en_US",
        "level": "",
        "loggedinlevel": "2",
        "passPhrase": "",
        "retypeDUPassPhrase": "",
        "adminLoggedInBank": "i18n/en_US",
        "formattedDate": "YY:MM:DD",
        "dateOrder": "1",
        "timeStampFormat": "yyyy-MM-dd hh:mm:ss a z",
        "recordsPerPage": "10",
        "symbolLink": "0",
        "symbolDisplay": "1",
        "dateFormat": "YY:MM:DD",
        "dateSeparator": "",
        "pwdUsagePolicy": "0",
        "tempPwdDuration": "2",
        "chPwdExpiryDuration": "0",
        "supportUpgradedCAPUI": "0",
        "enableAbridgedADS": "0",
        "enrollmentLockingPolicy": "0",
        "maxValForLastPwdList": "0",
        "promptNPasswordCharacters": "0",
        "enableRiskFort": null,
        "rfIntermediatePage": "0",
        "collectMCDDNA": "0",
        "cookieType": "0",
        "logVEReqPAReqTime": "0",
        "simultaneousCHLogin": "0",
        "enrollmentOfLockedCard": "0",
        "arcotOtpEnabled": "0",
        "authMinderOrgName": "",
        "arcotOtpImagesRelativePath": "",
        "arcotOTPProfileName": "",
        "arcotEMVAccountType": "",
        "maxTransactionsWithoutAOTP": "-1",
        "maxNonAOTPTxns": "0",
        "calloutAuthMethod": "0",
        "contextPath": "/vpas",
        "pareqWaitTimeBuffer": "120",
        "uaCheckEnabled": "0",
        "passwordStorageAlgorithm": "2",
        "authminderFailOverOptions": "0",
        "displayOptionalInfo": "0",
        "maxChangePwdTries": "0",
        "chPwdExpiry": "0",
        "velog": "0",
        "logPreviousTxnDetails": "0",
        "logRFStatus": "0",
        "enableCaseAutoAssignment": "0",
        "haWithBackupServers": "0",
        "enableAbridgedFyp": "0",
        "brandRFIntermediatePage": "0",
        "enableargus": "0",
        "raDownHandleAction": "0",
        "autoAssignmentForPriority": "0",
        "caseGroupingForPriority": "",
        "includeClosedCases": "",
        "caseGroupingForPriorityInCM": "",
        "emailPIIType": null,
        "pamPIIType": null,
        "issuerAnsPIIType": null,
        "amdsCustomerID": "",
        "amdsProviderName": "",
        "amdsFromNumber": "",
        "amdsFromEmail": "",
        "amdsAPINAME": "",
        "amdsProfileEmail": "",
        "amdsProfilePassword": "",
        "suppressCHContactDetailsInput": "0",
        "sendTempPasswordToCH": "1",
        "enableSCA": "NONE",
        "trustedBeneficiary": "NONE"
      }
    console.log(body);
    return this.http.post<any>("http://localhost:8080/vpas/api/issuer/editIssuerParamDetails", body, this.httpoptions)
 
  }

}

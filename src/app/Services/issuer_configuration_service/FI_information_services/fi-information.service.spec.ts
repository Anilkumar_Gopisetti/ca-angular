import { TestBed } from '@angular/core/testing';

import { FiInformationService } from './fi-information.service';

describe('FiInformationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FiInformationService = TestBed.get(FiInformationService);
    expect(service).toBeTruthy();
  });
});

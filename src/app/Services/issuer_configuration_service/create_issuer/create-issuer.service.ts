import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CreateIssuerService {
  url:string = "http://192.168.150.113:8081/vpas/api/issuer/addOnlyIssuer";
  constructor( private http:HttpClient ) { }

  getAllCountries(){
    return this.http.get<any>(this.url + 'getAllCountry');
  }

  getAllLocales(){
    return this.http.get<any>(this.url + 'getAllLocale');
  }

  getAllAdmins(){
    let body = {
      "bankId":9,
      "name":"global"
    }
    return this.http.post<any>(this.url + "getAllAdminsByLevel", body)
  }

  createIssuer(createIssuerData){

    {
      if (createIssuerData.changePasswordAtFirstLogin == true) {
        createIssuerData.changePasswordAtFirstLogin = "1";
      }
      else {
        createIssuerData.changePasswordAtFirstLogin = "0"
      }
    }
    {
      if (createIssuerData.pwdNeverExpires == true) {
        createIssuerData.pwdNeverExpires = "1";
      }
      else {
        createIssuerData.pwdNeverExpires = "0"
      }
    }
    {
      if (createIssuerData.blkSimultLogin == true) {
        createIssuerData.blockksimultlogin = "1";
      }
      else {
        createIssuerData.blockksimultlogin = "0"
      }

    }
    {
      if (createIssuerData.updateProfileAtLogin == true) {
        createIssuerData.updateProfileAtLogin = "1";
      }
      else {
        createIssuerData.updateProfileAtLogin = "0";
      }
    }

    if (createIssuerData.collectAdminContactDetails == 0) {
      createIssuerData.twoFactorDeliveryChannelType = 4;
    }



    // let body = 
    //   {
    //     "adminDto": {
    //       "collectAdminContactDetails": createIssuerData.collectAdminContactDetails,
    //       "deliveryChannel": 0
    //     },
    //     "bankDirName": createIssuerData.bankDirName,
    //     "bankId": 0,
    //     "bankKey": "anil",
    //     "bankName": createIssuerData.bankName ,
    //     "country": createIssuerData.country ,
    //     "customerId": createIssuerData.customerId ,
    //     "cvvDesKeyA": createIssuerData.cvvKeyA ,
    //     "cvvDesKeyB": createIssuerData.cvvKeyB ,
    //     "cvvKeyInd": createIssuerData.CVVKEYIND ,
    //     // "dateCreated": "stri" ,
    //     "doNotPromptBehavior": createIssuerData.doNotPromptBehavior ,
    //     "dualAuthRequiredAtXCommit": 0,
    //     "issuerDisplayName": createIssuerData.issuerDisplayName ,
    //     "localeId": createIssuerData.localeID ,
    //     "processorData": createIssuerData.processorData,
    //     "processorInfo": createIssuerData.processorInfo,
    //     "processorName": createIssuerData.processorName ,
    //     "reportTimeZoneLocal": "string",
    //     "selectedAdminList": [
    //       {
    //         "adminName": "string"
    //       }
    //     ],
    //     "selectedLocaleList": [
    //       {
    //         "description": "string",
    //         "locale": "selected",
    //         "localeId": createIssuerData.localeID
    //       }
    //     ],
    //     "subProcessorName": createIssuerData.subProcessorName,
    //     "twoStepLoginEnabled": 1,
    //     "uploadKey": createIssuerData.uploadKey,
    //     "userEncoding": createIssuerData.userEncoding
    //   }

    let body={
      
        "adminName": "XHEC18tn55UAPzKtu9cV6Q==",
        "nameDecrypted": "GLOBAL",
        "nameEncrypted": "XHEC18tn55UAPzKtu9cV6Q==",
        "action": "add",
        "privId" : "S6209",
        "bank": "test0987",
        "locale": "i18n/en_US",
        "loggedinlevel": "2",
        "level": "",
        "slt_selAdmins": ["GA1"],
        "bankName": "xyztmt",
        "issuerDisplayName": "abcxyz1",
        "uploadKey": "4354",
        "retypeUploadKey": "4354",
        "uploadKeyHidden": "",
        "cvvKeyA": "123498",
        "cvvKeyB": "456732",
        "processorName": "xyztmc1",
        "subProcessorName": "xyztmc1",
        "processorData": "xyztmc1",
        "processorInfo": "processorInfo",
        "CVVKEYIND": "01",
        "doNotPromptBehavior": "1",
        "localTimeZone": "(GMT+04:00)",
        "customerId": "",
        "userEncoding": "UTF-8",
        "bankKeyModule": "S/W",
        "arKeyModule": "",
        "userIdEnabled": "0",
        "auth_token": "",
        "twoStepLoginEnabled": "0",
        "dualAuthRequiredAtCommit": "0",
        "is2FAAuthenticationEnabled": "",
        "collectAdminContactDetails": "0",
        "twoFactorDeliveryChannelType": "0",
        "twoFactorAuthenticationType": "0",
        "AUTH_TOKEN_VALIDATED": true,
        "encryptionKey": "56743",
        "bankDir": "xyztmt123",
        "country": "50",
        "localeID": "1",
        "countryname": "",
        "enrollmentLocales": [3],
        "adminLoggedInBank": "i18n/en_US",
        "contextPath": "/vpas",
        "requestURI": "/vpas/admin/AddUpdateIssuer.jsp",
        "queryString": "privId=S6222&bank=i18n/en_US&locale=en_US&loggedinlevel=2",
        "acl_decrypted" : "S9200:S9201:S8350:S9202:S9203:S6210:S6211:IDS.ACCT.D:S6450:S7023:IDS.TDSTOKDETOK:S3904:S6214:S6215:S6212:S7026:S9204:S6218:S9601:S9205:S6216:S6217:IDS.LINK.W:S9312:S9313:S6442:S7011:S8188:S12154:S13001:S12155:S13002:S6209:S8909:S12153:IDS.TOKDETOK:S6204:S6201:S8985:S6202:S6207:S6208:S6205:S6206:S6470:S8492:S6232:S6233:S6230:S6990:S6231:S12143:S12144:FM.ENROLL:IDS.ENCDEC:S6994:S6999:S3403:S3402:S9900:S6997:S9851:S8400:S6221:S6101:S6222:S3471:S6220:IDS.ACCT.W:FM.AUTH:IDS.ACCT.R:S11200:S6225:S6501:S6226:S6984:S6102:S6223:S6983:S6103:S6224:S6982:S6229:S6989:S6988:S8923:S6227:S6228:S9882:S9240:S84921:S9881:S12115:FM.RISK:S6418:S3549:S6419:S6412:S9923:S8557:S6413:S6410:S6411:S6416:S6417:S6414:S8559:S9992:S9112:S8541:S9111:S6409:S6408:S8943:S6401:S6402:S3495:S6405:S9997:S6406:S6403:S6404:S9300:IDS.FACT.R:S6310:S6431:IDS.FACT.W:S12101:S6314:S6311:S6432:S6433:S6438:S6439:S6315:S6436:S6316:S6437:S9134:S9135:S6420:IRIS.IDSCONFIG.TOOL:S6308:S6429:S6309:S6302:S8327:S6303:S6421:S6301:S6427:S6428:IDS.FACT.D:S9810:S6304:S6305"
      
    }
    console.log(body);
    
      return this.http.post<any>(this.url, body);
  }
}

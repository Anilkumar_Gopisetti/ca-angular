import { TestBed } from '@angular/core/testing';

import { UploadSigningCertificateService } from './upload-signing-certificate.service';

describe('UploadSigningCertificateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadSigningCertificateService = TestBed.get(UploadSigningCertificateService);
    expect(service).toBeTruthy();
  });
});

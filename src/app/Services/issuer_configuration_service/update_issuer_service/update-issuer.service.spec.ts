import { TestBed } from '@angular/core/testing';

import { UpdateIssuerService } from './update-issuer.service';

describe('UpdateIssuerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UpdateIssuerService = TestBed.get(UpdateIssuerService);
    expect(service).toBeTruthy();
  });
});

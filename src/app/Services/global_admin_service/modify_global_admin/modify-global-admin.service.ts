import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ModifyGlobalAdminService {

  url: string = "http://192.168.150.111:8080/vpas/api/admin/"
  url1:string = "http://192.168.150.111:8080/vpas/api/admin/2"
  sessionData: any = JSON.parse(localStorage.getItem("sessionData"));
  token: any = this.sessionData.token;
  constructor(private http: HttpClient) { }
  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': this.token,
      'Content-Type': "application/json"
    })
  }

  getAllAdmins(){
    return this.http.get<any>(this.url1, this.httpoptions)
  }
  getAdminDetails(adminName){
    return this.http.get<any>(this.url + 'status/' + adminName + "/" + 0, this.httpoptions)
  }

  modifyGlobalAdmin(data) {
    let body = {

      "selectedAdminBank": "0",
      "selectedAdminName":data.selectedAdminName,
      "remark": data.remark,
      "level": "3",
      "action": "updateUser",
      "status": data.status,
      "locale": "en_US",
      "bank": "i18n/en_US",
      "adminForUpdate": data.selectedAdminName

    }
    return this.http.put<any>(this.url + "changestatus", body, this.httpoptions)
  }

}

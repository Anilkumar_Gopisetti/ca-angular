import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class UpdateGlobalAdminPrivilegesService {

  // url: string = "http://localhost:8080/vpas/api/admin/"
  url: string = "http://192.168.150.111:8080/vpas/api/admin/"

  sessionData: any = JSON.parse(localStorage.getItem("sessionData"));
  token: any = this.sessionData.token;

  constructor(private http: HttpClient) { }

  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': this.token,
      'Content-Type': "application/json"
    })
  }
  getallUsers() {
    return this.http.get<any>(this.url + "2", this.httpoptions);
  }
  
  getallbanks() {
    return this.http.get<any>(this.url + "banklist", this.httpoptions);
  }

 
  getAdminDetails(userId) {

    let body = {
      "locale": "en_US",
      "action": "changeAdmin",
      "adminIdClear": userId,
      "level": "2",
      "bank": "i18n/en_US"
    }
    return this.http.put<any>(this.url + "privilege", body, this.httpoptions);
  }

  updateGLobalAdminPrivileges(adminData) {
    console.log(adminData);
    
    let processerName;
    {if(adminData.processorName == null){
       processerName = "-1000"
    }
    else {
      processerName = adminData.processorName
    }
  }
    let subProcesserName;
    {if(adminData.subProcesserName == null){
      subProcesserName = "-1000"
    }
    else {
      subProcesserName = adminData.subProcesserName
    }}
    let body = {
      "locale": "en_US",
      "action": "saveAdmin",
      "adminIdClear": adminData.userEncoding,
      "adminBankId": "0",
      "firstNameClear": adminData.firstName,
      "middleNameClear": adminData.middleName,
      "lastNameClear":adminData.lastName,
      "description": adminData.description,
      "selectedBanks": adminData.slt_selIssuers,
      "blockkSimultaneousLogin": adminData.blksimultlogin,
      "updateProfileAtLogin": adminData.updateProfileAtLogin,
      "maxValForLastPwdList": adminData.maxValForLastPwdList.toString(),
      "passwordType": true,
      "subProcessorName": subProcesserName,
      "processorName": processerName,
      "twoFactorAuthenticationType": 1,
      "twoFactorDeliveryChannelType": adminData.twoFactorDeliveryChannelType,
      "twoFactorAuthenticationEnabled": true,
      "collectAdminContactDetails": adminData.collectAdminContactDetails,
      "passwordTypeChanged": true,
      "level": "2",
      "pwdNeverExpires": adminData.pwdNeverExpires,
      "privileges": adminData.privs,
      "bank": "i18n/en_US",
      "templateIdClear": adminData.templateIdClear
    }
    console.log(body);
      return this.http.put<any>(this.url + "privilege", body, this.httpoptions);
  }

}

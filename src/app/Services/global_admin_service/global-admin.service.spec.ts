import { TestBed } from '@angular/core/testing';

import { GlobalAdminService } from './global-admin.service';

describe('GlobalAdminService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GlobalAdminService = TestBed.get(GlobalAdminService);
    expect(service).toBeTruthy();
  });
});

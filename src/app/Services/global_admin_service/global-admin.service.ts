import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class GlobalAdminService {
  url: string = "http://192.168.150.111:8080/vpas/api/globaladmin/";
  getUrl: string = "http://192.168.150.111:8080/vpas/api/admin/";
  
  sessionData: any = JSON.parse(localStorage.getItem("sessionData"));
  token: any = this.sessionData.token;
  
  
  
  constructor(private http: HttpClient) { }
  httpoptions = {
  headers: new HttpHeaders({
  'Authorization': this.token,
  'Content-Type': "application/json"
  })
  }
  
  getAllCountries() {
  return this.http.get<any>(this.getUrl + "countrylist/1", this.httpoptions);
  }
  
  getAllIssuers() {
  return this.http.get<any>(this.getUrl + "banklist", this.httpoptions)
  }
  
  getAllPrivileges() {
  // let body = {
  // "adminName": this.sessionData.adminName
  // }
  return this.http.get<any>(this.url + "privileges", this.httpoptions);
  }
  getBanklist(value)
  {
  let body = {
  "level":2,
  "secondaryAdminName":"global",
  "bankId":"0"
  }
  return this.http.post<any>("http://192.168.150.111:8080/vpas/api/admin/issuers",body,this.httpoptions);
  }
  
  getTemplates() {
  let body = {
  "level":2,
  "adminNameForUpdate":localStorage.getItem('username')
  }
  return this.http.post<any>("http://192.168.150.111:8080/vpas/api/admin/authtemplates",body);
  }
  
  createGlobalAdmin(data) {
  {
  if (data.changePasswordAtFirstLogin == true) {
  data.changePasswordAtFirstLogin = "1";
  }
  else {
  data.changePasswordAtFirstLogin = "0"
  }
  }
  {
  if (data.pwdNeverExpires == true) {
  data.pwdNeverExpires = "1";
  }
  else {
  data.pwdNeverExpires = "0"
  }
  }
  {
  if (data.blkSimultLogin == true) {
  data.blockksimultlogin = "1";
  }
  else {
  data.blockksimultlogin = "0"
  }
  
  }
  {
  if (data.updateProfileAtLogin == true) {
  data.updateProfileAtLogin = "1";
  }
  else {
  data.updateProfileAtLogin = "0";
  }
  }
  
  if (data.collectAdminContactDetails == 0) {
  data.twoFactorDeliveryChannelType = 4;
  }
  
  
  let body = {
  "adminUserId": data.userid,
  "firstName": data.firstName,
  "lastName": data.lastName,
  "level": "2",
  "middleName": data.middleName,
  "desciption": data.description,
  "maxValForLastPwdList": data.maxValForLastPwdList,
  "pasword": data.password,
  "rePassword": data.rePassword,
  "changePasswordAtFirstLogin": data.changePasswordAtFirstLogin,
  "pwdNeverExpires": data.pwdNeverExpires,
  "blksimultlogin": data.blockksimultlogin,
  "updateProfileAtLogin": data.updateProfileAtLogin,
  "collectAdminContactDetails": data.collectAdminContactDetails.toString(),
  "twoFactorDeliveryChannelType": data.twoFactorDeliveryChannelType,
  "subProcessorName": data.subProcessorName,
  "selectedAdminBank": "0",
  "selectedTemplateName": data.selectedTemplateName,
  "countries": data.slt_selCountries,
  "bankList": data.slt_selIssuers,
  "successMsg": "0",
  "error": "0",
  "secAdminName": "global",
  "secAdminName_decrypted": "global",
  "multiControl": "false",
  // "listOfCountries": "",
  "errorPwdPolicy": "nilesh",
  "blkSimultLogin": data.blockksimultlogin,
  "is2FAAuthenticationEnabled": "0",
  "processorName": data.processorName,
  "twoFactorDelChnlType1": "0",
  "twoFactorAuthenticationType": "1",
  "deliveryChannel": "0",
  "privs": data.privs,
  "bank": "i18n/en_US",
  "action": "newUser"
  }
  console.log(body)
  return this.http.post<any>(this.url, body, this.httpoptions);
  }
  
  createGlobalAdminTemplate(data)
  {
  if (data.pwdNeverExpires == true) {
  data.pwdNeverExpires = "1";
  }
  else {
  data.pwdNeverExpires = "0"
  }
  let body = {
  "adminUserId":data.userid,
  "firstName":data.firstName,
  "lastName":data.lastName,
  "level":2,
  "middleName": data.middleName,
  "desciption":data.description,
  "pwdNeverExpires":data.pwdNeverExpires,
  "selectedAdminBank":"0",
  "selectedTemplateName":data.selectedTemplateName,
  "successMsg":"nilesh",
  "error":"nilesh",
  "secAdminName":"global",
  "secAdminName_decrypted":"global",
  "multiControl":"false",
  "errorPwdPolicy":"nilesh",
  "bankList":data.slt_selIssuers,
  "bank":"i18n/en_US",
  "action":"newUser"
  
  }
  return this.http.post<any>(this.url, body, this.httpoptions);
  }
  }
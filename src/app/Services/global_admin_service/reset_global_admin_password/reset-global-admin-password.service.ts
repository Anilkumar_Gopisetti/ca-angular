import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ResetGlobalAdminPasswordService {
  bankUrl: string = "http://192.168.150.111:8080/vpas/api/admin/2"
  resetUrl: string = "http://192.168.150.111:8080/vpas/api/admin/reset/password"
  sessionData: any = JSON.parse(localStorage.getItem("sessionData"));
  token: any = this.sessionData.token;
  constructor(private http: HttpClient) { }
  httpoptions = {
    headers: new HttpHeaders({
      'Authorization': this.token,
    })
  }

  getAllAdmins() {
    return this.http.get<any>(this.bankUrl, this.httpoptions)
  }
 
  resetGlobalAdmin(data) {
    let body = {
      "loggedInUserid":"global",
      "selectedBank":"0",
      "selectedAdmin":data.Admins,
      "password":data.newPassword,
      "remarks":data.remark,
      "level":2,
      "action":"updateUser",
      "reCheckPassword":data.retypeNewpassword,
      "changePwdAtLogin":"0",
      "locale":"en_US",
      "loggedInBank":"i18n/en_US",
      "emailCheck": "on",
      "phoneCheck": "on"
      }
    console.log(body);
    return this.http.put<any>(this.resetUrl, body, this.httpoptions)
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthServiceService {

  constructor(private http: HttpClient) { }

  url: string = "http://192.168.150.111:8080/vpas/api/";
  MasterUrl: string = "http://192.168.150.111:8080/api/admin/master/signin";

  doLogin(credentials) {
    credentials.bank = "i18n/en_US";
    credentials.locale = "en_US";
    credentials.roaming = "false";
    credentials.key = "masterKey";
    return this.http.post<any>(this.url + 'auth/login', credentials);
  }
  doMasterLogin(credentials) {
    return this.http.post<any>(this.MasterUrl, credentials)
  }
  dochangePassword(data) {
    data.bank = "i18n/en_US";
    data.locale = "en_US";
    data.attempts = "0";
    if (localStorage.getItem('entered') == "false") {
      data.oldPassword = localStorage.getItem('password');
    }
    data.loggedInAdminName = localStorage.getItem('username');
    return this.http.put<any>(this.url + "admin/change/password", data);
  }
}

import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable()
export class UpdateProfileService {
  url: string = "http://192.168.150.111:8080/vpas/api/"
  sessionData: any = JSON.parse(localStorage.getItem("sessionData"));
  token: any = this.sessionData.token;
  constructor(private http: HttpClient) { }
  httpoptions = {
    headers: new HttpHeaders({
      // 'Authorization':"Bearer "+ this.token,
      'Authorization': this.token,
      'Content-Type': "application/json"
    })
  }

  getProfileData(){
    return this.http.get<any>(this.url + "admin/loggedinadmin/0", this.httpoptions)
  }

  getAllTimeZones() {
    return this.http.get("../../../assets/constants/timeZoneList.json")
  }

  updateAdminProfile(data) {
    let body = {
      "bank": "i18n/en_US",
      "roaming": "false",
      "locale": "en_US",
      "loggedinlevel": "2",
      "adminEmailAddress": data.adminEmailAddress,
      "adminPhoneNumber": data.adminPhoneNumber,
      "action": "submit",
      "jobTitle": data.jobTitle,
      "supervisorName": data.supervisorName,
      "adminHintQuestion": data.adminHintQuestion,
      "adminHintAnswer1": data.adminHintAnswer1,
      "adminHintAnswer2": data.adminHintAnswer1,
      "recordsPerPage": data.recordsPerPage,
      "adminLocale": "en_US",
      "localTimeZone": data.localTimeZone,
      "timeStampFormat": data.timeStampFormat,
      "startdate": data.startdate,
      "startDateOffset": data.startDateOffset,
      "privId": "",
      "level": "2"
    }
    return this.http.put<any>(this.url + "globaladmin/firstlogin", body, this.httpoptions)
  }
}

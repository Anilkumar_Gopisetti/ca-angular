import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginpageComponent } from './components/Global Admin/loginpage/loginpage.component';
import { SidenavComponent } from './components/commonFiles/sidenav/sidenav.component';
import { HeaderComponent } from './components/commonFiles/header/header.component';
import { IndexComponent } from './components/commonFiles/index/index.component';
import { CreateIssuerComponent } from './components/Global Admin/Issuer Configurations/create-issuer/create-issuer.component';
import { CreateGlobalAdminComponent } from './components/Global Admin/Global admin configurations/create-global-admin/create-global-admin.component';
import { ModifyGlobalAdminComponent } from './components/Global Admin/Global admin configurations/modify-global-admin/modify-global-admin.component';
import { ResetGlobalAdminComponent } from './components/Global Admin/Global admin configurations/reset-global-admin/reset-global-admin.component';
import { UpdateGlobalAdminComponent } from './components/Global Admin/Global admin configurations/update-global-admin/update-global-admin.component';
import { AddFIInformationComponent } from './components/Global Admin/Issuer Configurations/add-fi-information/add-fi-information.component';
import { UpdateFiInformationComponent } from './components/Global Admin/Issuer Configurations/update-fi-information/update-fi-information.component';
import { AddIssuerCustomizationComponent } from './components/Global Admin/Issuer Configurations/add-issuer-customization/add-issuer-customization.component';
import { MasterAdminLoginComponent } from './components/Master Admin/master-admin-login/master-admin-login.component';
import { ConfigureGlobalAdminPolicyComponent } from './components/Master Admin/configure-global-admin-policy/configure-global-admin-policy.component';
import { CreateGlobalAdmin1Component } from './components/Master Admin/create-global-admin1/create-global-admin1.component';
import { UpdateIssuerComponent } from './components/Global Admin/Issuer Configurations/update-issuer/update-issuer.component';
import { UploadSigningCertificateComponent } from './components/Global Admin/Issuer Configurations/upload-signing-certificate/upload-signing-certificate.component';
import { EnableCardRangesComponent } from './components/Global Admin/Issuer Configurations/enable-card-ranges/enable-card-ranges.component';
import { ChangePasswordComponent } from './components/commonFiles/change-password/change-password.component';
import { HelpComponent } from './components/commonFiles/help/help.component';
import { UpdateProfileComponent } from './components/commonFiles/update-profile/update-profile.component';
import { CreateIssuerAdminComponent } from './components/Global Admin/Issuer Admin Configurations/create-issuer-admin/create-issuer-admin.component';
import { ModifyIssuerAdminAccountComponent } from './components/Global Admin/Issuer Admin Configurations/modify-issuer-admin-account/modify-issuer-admin-account.component';
import { ResetIssuerAdminPasswordComponent } from './components/Global Admin/Issuer Admin Configurations/reset-issuer-admin-password/reset-issuer-admin-password.component';
import { UpdateIssuerAdminPrevilegesComponent } from './components/Global Admin/Issuer Admin Configurations/update-issuer-admin-previleges/update-issuer-admin-previleges.component';
import { ConfigureIssuerAdminPrevilegesComponent } from './components/Global Admin/Issuer Admin Configurations/configure-issuer-admin-previleges/configure-issuer-admin-previleges.component';
import { ConfigureIssuerAdminPolicyComponent } from './components/Global Admin/Issuer Admin Configurations/configure-issuer-admin-policy/configure-issuer-admin-policy.component';
import { ViewAndUpdateCaseComponent } from './components/Global Admin/case management/view-and-update-case/view-and-update-case.component';
import { ConfigureIssuerParametersComponent } from './components/Global Admin/Issuer Configurations/configure-issuer-parameters/configure-issuer-parameters.component';
import { FormInfoComponent } from './components/commonFiles/form-info/form-info.component';
import { AuthServiceService } from './Services/auth_service/auth-service.service';
import { ErrorMessagesService } from './Services/service_for_errorMessages/error-messages.service';
import { ModifyGlobalAdminService } from './Services/global_admin_service/modify_global_admin/modify-global-admin.service';
import { ResetGlobalAdminPasswordService } from './Services/global_admin_service/reset_global_admin_password/reset-global-admin-password.service';
import { UpdateGlobalAdminPrivilegesService } from './Services/global_admin_service/update_global_admin_privileges/update-global-admin-privileges.service';
import { GlobalAdminService } from './Services/global_admin_service/global-admin.service';
import { ConfigureIssuerAdminPolicyService } from './Services/issuer_admin_configuration_service/configure_issuer_admin_policy/configure-issuer-admin-policy.service';
import { ConfigureIssuerAdminPrivilegesService } from './Services/issuer_admin_configuration_service/configure_issuer_admin_privileges/configure-issuer-admin-privileges.service';
import { CreateIssuerAdminService } from './Services/issuer_admin_configuration_service/create_issuer_admin/create-issuer-admin.service';
import { ModifyIssuerAdminService } from './Services/issuer_admin_configuration_service/modify_issuer_admin/modify-issuer-admin.service';
import { ResetIssuerAdminPasswordService } from './Services/issuer_admin_configuration_service/reset_issuer_admin_password/reset-issuer-admin-password.service';
import { UpdateIssuerAdminPrivilegesService } from './Services/issuer_admin_configuration_service/update_issuer_admin_privileges/update-issuer-admin-privileges.service';
import { CreateIssuerService } from './Services/issuer_configuration_service/create_issuer/create-issuer.service';
import { FiInformationService } from './Services/issuer_configuration_service/FI_information_services/fi-information.service';
import { UpdateProfileService } from './Services/update_profile_service/update-profile.service';
import { AddIssuerCustomizationService } from './Services/issuer_configuration_service/add_issuer_customization_service/add-issuer-customization.service';
import { ConfigureIssuerParametersService } from './Services/issuer_configuration_service/configure_issuer_parameters_services/configure-issuer-parameters.service';
import { EnableCardRangesService } from './Services/issuer_configuration_service/enable_card_ranges_service/enable-card-ranges.service';
import { UpdateFiInformationService } from './Services/issuer_configuration_service/update_FI_information_service/update-fi-information.service';
import { UpdateIssuerService } from './Services/issuer_configuration_service/update_issuer_service/update-issuer.service';
import { UploadSigningCertificateService } from './Services/issuer_configuration_service/upload_signing_certificate_service/upload-signing-certificate.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginpageComponent,
    SidenavComponent,
    HeaderComponent,
    IndexComponent,
    CreateIssuerComponent,
    CreateGlobalAdminComponent,
    ModifyGlobalAdminComponent,
    ResetGlobalAdminComponent,
    UpdateGlobalAdminComponent,
    AddFIInformationComponent,
    UpdateFiInformationComponent,
    AddIssuerCustomizationComponent,
    MasterAdminLoginComponent,
    ConfigureGlobalAdminPolicyComponent,
    CreateGlobalAdmin1Component,
    UpdateIssuerComponent,
    UploadSigningCertificateComponent,
    EnableCardRangesComponent,
    ChangePasswordComponent,
    HelpComponent,
    UpdateProfileComponent,
    CreateIssuerAdminComponent,
    ModifyIssuerAdminAccountComponent,
    ResetIssuerAdminPasswordComponent,
    UpdateIssuerAdminPrevilegesComponent,
    ConfigureIssuerAdminPrevilegesComponent,
    ConfigureIssuerAdminPolicyComponent,
    ViewAndUpdateCaseComponent,
    ConfigureIssuerParametersComponent,
    FormInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    AuthServiceService,
    ErrorMessagesService,
    ModifyGlobalAdminService,
    ResetGlobalAdminPasswordService,
    UpdateGlobalAdminPrivilegesService,
    GlobalAdminService,
    ConfigureIssuerAdminPolicyService,
    ConfigureIssuerAdminPrivilegesService,
    CreateIssuerAdminService,
    ModifyIssuerAdminService,
    ResetIssuerAdminPasswordService,
    UpdateIssuerAdminPrivilegesService,
    CreateIssuerService,
    FiInformationService,
    UpdateProfileService,
    AddIssuerCustomizationService,
    ConfigureIssuerParametersService,
    EnableCardRangesService,
    UpdateFiInformationService,
    UpdateIssuerService,
    UploadSigningCertificateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
